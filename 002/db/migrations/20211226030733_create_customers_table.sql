-- migrate:up
create table customers (
    id integer not null primary key autoincrement,
    name varchar(45) not null,
    phone varchar(45) null,
    created_at datetime not null default current_timestamp,
    updated_at datetime null
);

create trigger customers_trig after update on customers
begin
    update customers set updated_at = datetime('now') where id = NEW.id;
end;

-- migrate:down

drop trigger if exists customers_trig;

drop table customers;
