CREATE TABLE IF NOT EXISTS "schema_migrations" (version varchar(255) primary key);
CREATE TABLE sqlite_sequence(name,seq);
CREATE TABLE customers (
    id integer not null primary key autoincrement,
    name varchar(45) not null,
    phone varchar(45) null,
    created_at datetime not null default current_timestamp,
    updated_at datetime null
);
CREATE TRIGGER customers_trig after update on customers
begin
    update customers set updated_at = datetime('now') where id = NEW.id;
end;
-- Dbmate schema migrations
INSERT INTO "schema_migrations" (version) VALUES
  ('20211226030733');
