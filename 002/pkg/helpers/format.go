package helpers

import (
	"database/sql"
	"time"
)

func Int64ToNullInt64(i int64) sql.NullInt64 {
	return sql.NullInt64{
		Int64: i,
		Valid: true,
	}
}

func StringToNullString(s string) sql.NullString {
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}

func TimeToNullTime(t time.Time) sql.NullTime {
	return sql.NullTime{
		Time:  t,
		Valid: true,
	}
}

func NullTimeToTime(t sql.NullTime) time.Time {
	return t.Time
}

func HumanizeTime(t time.Time) string {
	if t.IsZero() {
		return ""
	}

	return t.Format(time.RFC3339)
}

func NullStringToString(s sql.NullString) string {
	if s.Valid {
		return s.String
	}

	return ""
}
