package main

import (
	"context"
	"database/sql"
	"erp/pkg/helpers"
	"erp/pkg/models"
	"github.com/gofiber/fiber/v2"
	"github.com/spf13/cast"
)

type Customer struct {
	ID    int64  `json:"id"`
	Name  string `json:"name"`
	Phone string `json:"phone"`

	CreatedAt string `json:"created_at"`
	UpdatedAt string `json:"updated_at"`
}

func ListCustomers(db *sql.DB) func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {

		selectQuery := `select id, name, phone, created_at, updated_at from customers`

		customContext, cancel := context.WithCancel(context.Background())
		defer cancel()

		rows, err := db.QueryContext(customContext, selectQuery)
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
		}
		defer rows.Close()

		var customers []Customer

		for rows.Next() {
			var customer models.Customer

			var createdAt, updatedAt sql.NullTime

			err := rows.Scan(&customer.ID, &customer.Name, &customer.Phone, &createdAt, &updatedAt)
			if err != nil {
				// TODO: need to add error log
				continue
			}

			createdAtStr := helpers.HumanizeTime(helpers.NullTimeToTime(createdAt))
			updatedAtStr := helpers.HumanizeTime(helpers.NullTimeToTime(updatedAt))

			customers = append(customers, Customer{
				ID:        cast.ToInt64(customer.ID),
				Name:      customer.Name,
				Phone:     helpers.NullStringToString(customer.Phone),
				CreatedAt: createdAtStr,
				UpdatedAt: updatedAtStr,
			})
		}

		return c.Status(fiber.StatusOK).JSON(&fiber.Map{
			"items": customers,
		})
	}
}

func DeleteCustomer(db *sql.DB) func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		id, err := c.ParamsInt("id")
		if err != nil {
			return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
		}

		customContext, cancel := context.WithCancel(context.Background())
		defer cancel()

		customer, err := models.CustomerByID(customContext, db, id)
		if err != nil {
			return c.Status(fiber.StatusNotFound).JSON(&fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
		}

		err = customer.Delete(customContext, db)
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
		}

		return c.Status(fiber.StatusNoContent).Send(nil)
	}
}

func UpdateCustomer(db *sql.DB) func(c *fiber.Ctx) error {
	return func(c *fiber.Ctx) error {

		id, err := c.ParamsInt("id")
		if err != nil {
			return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
		}

		customerReq := new(Customer)

		if err := c.BodyParser(customerReq); err != nil {
			return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
		}

		customContext, cancel := context.WithCancel(context.Background())
		defer cancel()

		customer, err := models.CustomerByID(customContext, db, id)
		if err != nil {
			return c.Status(fiber.StatusNotFound).JSON(&fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
		}

		customer.Name = customerReq.Name
		customer.Phone = helpers.StringToNullString(customerReq.Phone)

		err = customer.Save(customContext, db)
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
		}

		customerResp := Customer{
			ID:    cast.ToInt64(customer.ID),
			Name:  customer.Name,
			Phone: helpers.NullStringToString(customer.Phone),
		}

		return c.Status(fiber.StatusOK).JSON(customerResp)
	}
}

func NewCustomer(db *sql.DB) func(c *fiber.Ctx) error {

	return func(c *fiber.Ctx) error {

		customerReq := new(Customer)

		if err := c.BodyParser(customerReq); err != nil {
			return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
		}

		customer := models.Customer{
			Name:  customerReq.Name,
			Phone: helpers.StringToNullString(customerReq.Phone),
		}

		customContext, cancel := context.WithCancel(context.Background())
		defer cancel()

		if err := customer.Save(customContext, db); err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
				"status":  "fail",
				"message": err.Error(),
			})
		}

		customerReq.ID = cast.ToInt64(customer.ID)

		return c.Status(fiber.StatusOK).JSON(customerReq)
	}
}
