package main

import (
	"flag"
	"fmt"
	"github.com/gofiber/fiber/v2"
	_ "github.com/mattn/go-sqlite3"
	"github.com/xo/dburl"
	"log"
	"os"
)

func run() error {
	addr := flag.String("addr", ":4000", "default port for web service")

	dsn := flag.String("dsn", "file:db/database.sqlite3?loc=auto", "sqlite3 data source")

	flag.Parse()

	fmt.Println(*dsn)

	//TODO: validate dsn

	db, err := dburl.Open(*dsn)
	if err != nil {
		return err
	}

	app := fiber.New()

	app.Get("/", func(ctx *fiber.Ctx) error {
		return ctx.SendString("hello, world!")
	})

	group := app.Group("api")

	group.Get("/customers", ListCustomers(db))
	group.Post("/customers", NewCustomer(db))
	group.Put("/customers/:id", UpdateCustomer(db))
	group.Delete("/customers/:id", DeleteCustomer(db))

	return app.Listen(*addr)
}

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}
