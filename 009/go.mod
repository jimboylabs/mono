module watermq

go 1.18

require (
	github.com/ThreeDotsLabs/watermill v1.2.0-rc.7
	github.com/ThreeDotsLabs/watermill-amqp/v2 v2.0.6
)

require (
	github.com/cenkalti/backoff/v3 v3.2.2 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/lithammer/shortuuid/v3 v3.0.7 // indirect
	github.com/oklog/ulid v1.3.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rabbitmq/amqp091-go v1.2.0 // indirect
)
