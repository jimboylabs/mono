package main

import (
	"encoding/json"
	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill-amqp/v2/pkg/amqp"
	"github.com/ThreeDotsLabs/watermill/message"
	"watermq"
)

var (
	amqpURI = "amqp://guest:guest@localhost:5672/"
	logger  = watermill.NewStdLogger(false, false)
)

func main() {
	amqpConfig := amqp.NewDurableQueueConfig(amqpURI)

	publisher, err := amqp.NewPublisher(amqpConfig, logger)
	if err != nil {
		panic(err)
	}

	menu := watermq.Menu{Name: "testing 1"}

	payload1, _ := json.Marshal(menu)

	publisher.Publish("create_menu", message.NewMessage(watermill.NewUUID(), payload1))

	menu2 := watermq.Menu{Name: "testing 2"}

	payload2, _ := json.Marshal(menu2)

	publisher.Publish("update_menu", message.NewMessage(watermill.NewUUID(), payload2))
}
