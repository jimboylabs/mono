package main

import (
	"context"
	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill-amqp/v2/pkg/amqp"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/ThreeDotsLabs/watermill/message/router/middleware"
	"github.com/ThreeDotsLabs/watermill/message/router/plugin"
	"time"
)

var (
	amqpURI = "amqp://guest:guest@localhost:5672/"
	logger  = watermill.NewStdLogger(false, false)
)

func main() {
	amqpConfig := amqp.NewDurableQueueConfig(amqpURI)

	router, err := message.NewRouter(message.RouterConfig{}, logger)
	if err != nil {
		panic(err)
	}

	// SignalsHandler will gracefully shutdown Router when SIGTERM is received.
	// You can also close the router by just calling `r.Close()`.
	router.AddPlugin(plugin.SignalsHandler)

	// Router level middleware are executed for every message sent to the router
	router.AddMiddleware(
		// CorrelationID will copy the correlation id from the incoming message's metadata to the produced messages
		middleware.CorrelationID,

		// The handler function is retried if it returns an error.
		// After MaxRetries, the message is Nacked and it's up to the PubSub to resend it.
		middleware.Retry{
			MaxRetries:      3,
			InitialInterval: time.Millisecond * 100,
			Logger:          logger,
		}.Middleware,

		// Recoverer handles panics from handlers.
		// In this case, it passes them as errors to the Retry middleware.
		middleware.Recoverer,
	)

	subscriber, err := amqp.NewSubscriber(amqpConfig, logger)
	if err != nil {
		panic(err)
	}

	publisher, err := amqp.NewPublisher(amqpConfig, logger)
	if err != nil {
		panic(err)
	}

	router.AddHandler(
		"handler_create_menu",
		"create_menu",
		subscriber,
		"create_menu_processed",
		publisher,
		createMenuHandler{}.Handler,
	)

	router.AddHandler("handler_update_menu",
		"update_menu",
		subscriber,
		"update_menu_processed",
		publisher,
		updateMenuHandler{}.Handler)

	if err := router.Run(context.Background()); err != nil {
		panic(err)
	}
}
