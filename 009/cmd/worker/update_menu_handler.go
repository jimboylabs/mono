package main

import (
	"encoding/json"
	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill/message"
	"log"
	"watermq"
)

type updateMenuHandler struct {
	// put database connection here
}

func (c updateMenuHandler) Handler(msg *message.Message) ([]*message.Message, error) {
	menu := watermq.Menu{}

	err := json.Unmarshal(msg.Payload, &menu)
	if err != nil {
		return nil, err
	}

	log.Println("Update Menu Handler received message", msg.UUID, menu.Name)

	msg = message.NewMessage(watermill.NewUUID(), []byte("message produced by structHandler"))
	return message.Messages{msg}, nil
}
