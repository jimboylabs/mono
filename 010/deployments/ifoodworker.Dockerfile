FROM docker.io/golang:1.17-alpine as build

RUN apk update && apk add git

WORKDIR /app

COPY . .

RUN go build ifood/cmd/ifoodworker

FROM docker.io/alpine:latest AS final

EXPOSE 3333

COPY --from=build /app/.env.tilt /.env
COPY --from=build /app/ifoodworker /ifoodworker

ENTRYPOINT ["/ifoodworker"]
