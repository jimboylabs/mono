FROM docker.io/node:17-alpine AS bundler

WORKDIR /build

COPY cmd/ifoodweb .

RUN npm install

RUN npx tailwindcss -i ./resources/input.css -o ./static/output.css

FROM docker.io/golang:1.17-alpine as builder

RUN apk update && apk add git

WORKDIR /app

COPY . .
COPY --from=bundler /build/static/output.css /app/cmd/ifoodweb/static/output.css

RUN go build ifood/cmd/ifoodweb

FROM docker.io/alpine:latest AS final

EXPOSE 3333

COPY --from=builder /app/.env.tilt /.env
COPY --from=builder /app/ifoodweb /ifoodweb

ENTRYPOINT ["/ifoodweb"]
