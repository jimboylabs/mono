module ifood

go 1.17

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-chi/render v1.0.1
	github.com/go-sql-driver/mysql v1.6.0
	github.com/hibiken/asynq v0.22.1
	github.com/joho/godotenv v1.4.0
	github.com/spf13/cast v1.4.1
	github.com/xo/dburl v0.9.0
)

require (
	github.com/cespare/xxhash/v2 v2.1.1 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/go-redis/redis/v8 v8.11.2 // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/google/uuid v1.2.0 // indirect
	github.com/robfig/cron/v3 v3.0.1 // indirect
	golang.org/x/sys v0.0.0-20210112080510-489259a85091 // indirect
	golang.org/x/time v0.0.0-20190308202827-9d24e82272b4 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
