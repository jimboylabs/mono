package main

import (
	"embed"
	"ifood/internal/listmerchants"
	"log"
	"net/http"
	"text/template"
)

//go:embed html
var indexHTML embed.FS

type HomePage struct {
	errorLog *log.Logger

	merchantStore listmerchants.Store
}

func NewHomePage(errorLog *log.Logger, mstore listmerchants.Store) *HomePage {
	return &HomePage{errorLog: errorLog, merchantStore: mstore}
}

func (p *HomePage) Render(w http.ResponseWriter, r *http.Request) {
	type templateData struct {
		Merchants []*listmerchants.Merchant
	}

	req := &listmerchants.ListMerchantsRequest{
		Count:  10,
		Cursor: "1",
	}

	resp, err := p.merchantStore.Query(r.Context(), req)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	data := templateData{Merchants: resp.Merchants}

	ts, err := template.ParseFS(indexHTML, "html/home.page.tmpl")
	if err != nil {
		p.errorLog.Fatal(err)
	}

	err = ts.Execute(w, data)
	if err != nil {
		p.errorLog.Println(err.Error())
		http.Error(w, "Internal Server Error", 500)
	}
}
