package main

import (
	"embed"
	"errors"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/joho/godotenv"
	"github.com/xo/dburl"
	"ifood/internal/listmerchants"
	"io"
	"log"
	"mime"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"

	_ "github.com/go-sql-driver/mysql"
)

//go:embed static
var staticFiles embed.FS

func tryRead(fs embed.FS, prefix, requestedPath string, w http.ResponseWriter) error {
	f, err := fs.Open(path.Join(prefix, requestedPath))
	if err != nil {
		return err
	}
	defer f.Close()

	stat, _ := f.Stat()
	if stat.IsDir() {
		return errors.New("path is dir")
	}

	contentType := mime.TypeByExtension(filepath.Ext(requestedPath))
	w.Header().Set("Content-Type", contentType)
	_, err = io.Copy(w, f)
	return err
}

func run() error {
	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	err := godotenv.Load()
	if err != nil {
		errorLog.Println(err.Error())
		return err
	}

	db, err := dburl.Open(os.Getenv("DATABASE_URL"))
	if err != nil {
		errorLog.Println(err.Error())
		return err
	}

	listMerchantsStore := listmerchants.NewMySQLStore(errorLog, db)

	home := NewHomePage(errorLog, listMerchantsStore)

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	r.Get("/", home.Render)
	r.NotFound(func(w http.ResponseWriter, r *http.Request) {
		path := strings.TrimPrefix(r.URL.Path, "/static/")

		err := tryRead(staticFiles, "static", path, w)
		if err != nil {
			errorLog.Println(err)
			return
		}
	})

	addr := "0.0.0.0:8888"

	// start http server
	server := &http.Server{Addr: addr, Handler: r, ErrorLog: errorLog}

	infoLog.Printf("Starting server on %s\n", addr)
	err = server.ListenAndServe()
	if err != nil {
		errorLog.Println(err.Error())
		return err
	}

	return nil
}

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}
