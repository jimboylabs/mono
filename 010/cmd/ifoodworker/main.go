package main

import (
	"fmt"
	"github.com/hibiken/asynq"
	"github.com/joho/godotenv"
	"ifood/cmd/ifoodworker/tasks"
	"log"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

func run() error {
	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	err := godotenv.Load()
	if err != nil {
		errorLog.Println(err.Error())
		return err
	}

	redisAddr := fmt.Sprintf("%s:%s", os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PORT"))

	srv := asynq.NewServer(asynq.RedisClientOpt{
		Addr: redisAddr,
	}, asynq.Config{
		Concurrency: 10,
		Queues: map[string]int{
			"critical": 6,
			"default":  3,
			"low":      1,
		},
	})

	mux := asynq.NewServeMux()
	mux.Handle(tasks.TypeMerchantCreate, &tasks.MerchantCreateProcessor{InfoLog: infoLog})

	if err := srv.Run(mux); err != nil {
		log.Fatalf("could not run worker server: %v", err)
	}

	return nil
}

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}
