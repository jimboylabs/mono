package tasks

import (
	"context"
	"github.com/hibiken/asynq"
	"log"
)

const TypeMerchantCreate = "merchant_create"

type MerchantCreateProcessor struct {
	InfoLog *log.Logger
}

func (p *MerchantCreateProcessor) ProcessTask(ctx context.Context, t *asynq.Task) error {
	p.InfoLog.Printf("executing task: %s", t.Type())
	return nil
}
