package main

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/render"
	"github.com/joho/godotenv"
	"github.com/xo/dburl"
	"ifood/internal/createmenu"
	"ifood/internal/createmerchant"
	"ifood/internal/listmenus"
	"ifood/internal/listmerchants"
	"ifood/internal/services"
	"log"
	"net/http"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

func run() error {
	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stderr, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	err := godotenv.Load()
	if err != nil {
		errorLog.Println(err.Error())
		return err
	}

	db, err := dburl.Open(os.Getenv("DATABASE_URL"))
	if err != nil {
		errorLog.Println(err.Error())
		return err
	}

	taskClient := services.NewTaskClient(fmt.Sprintf("%s:%s", os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PORT")))

	createMerchantStore := createmerchant.NewMySQLStore(db)
	createMerchantEndpoint := createmerchant.NewEndpoint(infoLog, errorLog, createMerchantStore, taskClient)

	listMerchantsStore := listmerchants.NewMySQLStore(errorLog, db)
	listMerchantsEndpoint := listmerchants.NewEndpoint(infoLog, errorLog, listMerchantsStore)

	createMenuStore := createmenu.NewMySQLStore(db)
	createMenuEndpoint := createmenu.NewEndpoint(infoLog, errorLog, createMenuStore)

	listMenusStore := listmenus.NewMySQLStore(errorLog, db)
	listMenusEndpoint := listmenus.NewEndpoint(infoLog, errorLog, listMenusStore)

	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(render.SetContentType(render.ContentTypeJSON))

	r.Route("/api", func(r chi.Router) {
		createmerchant.SetupRoutes(r, createMerchantEndpoint)
		listmerchants.SetupRoutes(r, listMerchantsEndpoint)

		createmenu.SetupRoutes(r, createMenuEndpoint)
		listmenus.SetupRoutes(r, listMenusEndpoint)
	})

	addr := "0.0.0.0:3333"

	// start http server
	server := &http.Server{Addr: addr, Handler: r, ErrorLog: errorLog}

	infoLog.Printf("Starting server on %s\n", addr)
	err = server.ListenAndServe()
	if err != nil {
		errorLog.Println(err.Error())
		return err
	}

	return nil
}

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
	}
}
