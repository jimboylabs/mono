package createmenu

import (
	"errors"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/spf13/cast"
	"ifood/internal/common"
	"log"
	"net/http"
)

type Endpoint struct {
	infoLog  *log.Logger
	errorLog *log.Logger

	menuStore Store
}

func NewEndpoint(infoLog, errorLog *log.Logger, menuStore Store) *Endpoint {
	return &Endpoint{
		infoLog:   infoLog,
		errorLog:  errorLog,
		menuStore: menuStore,
	}
}

func SetupRoutes(r chi.Router, e *Endpoint) {
	r.Post("/create-menu", e.handle)
}

func (e *Endpoint) handle(w http.ResponseWriter, r *http.Request) {
	request := &CreateMenuRequest{}

	err := render.Bind(r, request)
	if err != nil {
		e.errorLog.Println(err.Error())
		render.Status(r, http.StatusBadRequest)
		render.JSON(w, r, common.NewErrResponse(http.StatusBadRequest, err.Error()))
		return
	}

	if err := e.menuStore.GetMerchantByID(r.Context(), cast.ToUint64(request.MerchantID)); err != nil {
		if errors.Is(err, ErrMerchantNotFound) {
			render.Status(r, http.StatusNotFound)
			render.JSON(w, r, common.NewErrResponse(http.StatusNotFound, err.Error()))
			return
		}

		render.Status(r, http.StatusInternalServerError)
		render.JSON(w, r, common.NewErrResponse(http.StatusInternalServerError, ""))
		return
	}

	response, err := e.menuStore.Create(r.Context(), request)
	if err != nil {
		e.errorLog.Println(err.Error())
		render.Status(r, http.StatusInternalServerError)
		render.JSON(w, r, common.NewErrResponse(http.StatusInternalServerError, err.Error()))
		return
	}

	render.Status(r, http.StatusCreated)
	render.JSON(w, r, response)
	return
}
