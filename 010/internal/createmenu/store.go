package createmenu

import (
	"context"
	"database/sql"
	"errors"
	"github.com/spf13/cast"
	"ifood/internal/common"
	"ifood/internal/model/mysql"
)

var (
	ErrMerchantNotFound = errors.New("merchant not found")
)

type Store interface {
	Create(ctx context.Context, request *CreateMenuRequest) (*CreateMenuResponse, error)
	GetMerchantByID(ctx context.Context, merchantID uint64) error
}

var _ Store = (*MySQLStore)(nil)

type MySQLStore struct {
	db *sql.DB
}

func NewMySQLStore(db *sql.DB) *MySQLStore {
	return &MySQLStore{db: db}
}

func (m *MySQLStore) GetMerchantByID(ctx context.Context, merchantID uint64) error {
	_, err := mysql.MerchantByID(ctx, m.db, merchantID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return ErrMerchantNotFound
		}
	}

	return nil
}

func (m *MySQLStore) Create(ctx context.Context, request *CreateMenuRequest) (*CreateMenuResponse, error) {
	menu := mysql.Menu{
		Name:        request.Name,
		MerchantID:  cast.ToUint64(request.MerchantID),
		Description: common.StringToNullString(request.Description),
		Price:       common.Int64ToNullInt64(request.Price),
	}

	err := menu.Save(ctx, m.db)
	if err != nil {
		return nil, err
	}

	resp := &CreateMenuResponse{
		ID:                cast.ToString(menu.ID),
		CreateMenuRequest: *request,
	}

	return resp, nil
}
