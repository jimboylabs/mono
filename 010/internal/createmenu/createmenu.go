package createmenu

import (
	"errors"
	"github.com/go-chi/render"
	"net/http"
)

var _ render.Binder = (*CreateMenuRequest)(nil)

type CreateMenuRequest struct {
	Name        string `json:"name"`
	Kind        string `json:"kind"`
	Description string `json:"description"`
	MerchantID  string `json:"merchant_id"`

	Price int64 `json:"price"`
}

func (c *CreateMenuRequest) Bind(r *http.Request) error {
	if c.Name == "" {
		return errors.New("missing required name field.")
	}

	if c.Kind == "" {
		return errors.New("missing required kind field.")
	}

	if c.MerchantID == "" {
		return errors.New("missing required merchant field.")
	}

	if c.Price == 0 {
		return errors.New("missing required price field.")
	}

	return nil
}

var _ render.Renderer = (*CreateMenuResponse)(nil)

type CreateMenuResponse struct {
	ID string `json:"id"`
	CreateMenuRequest
}

func (c *CreateMenuResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}
