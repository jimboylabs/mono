package services

import (
	"encoding/json"
	"github.com/hibiken/asynq"
)

type TaskClient struct {
	client *asynq.Client
}

type task struct {
	client  *TaskClient
	payload interface{}
	kind    string
}

func NewTaskClient(redisAddr string) *TaskClient {
	conn := asynq.RedisClientOpt{
		Addr:     redisAddr,
		Password: "",
	}

	return &TaskClient{client: asynq.NewClient(conn)}
}

func (c *TaskClient) Close() error {
	return c.client.Close()
}

func (c *TaskClient) New(kind string) *task {
	return &task{
		client: c,
		kind:   kind,
	}
}

func (t *task) Payload(payload interface{}) *task {
	t.payload = payload
	return t
}

func (t *task) Save() error {
	var err error

	var payload []byte
	if t.payload != nil {
		if _, err := json.Marshal(t.payload); err != nil {
			return err
		}
	}

	task := asynq.NewTask(t.kind, payload)

	_, err = t.client.client.Enqueue(task)

	return err
}
