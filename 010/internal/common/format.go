package common

import (
	"database/sql"
	"time"
)

func NullStringToString(v sql.NullString) string {
	if !v.Valid {
		return ""
	}

	return v.String
}

func StringToNullString(v string) sql.NullString {
	return sql.NullString{Valid: true, String: v}
}

func NullTimeToTime(v sql.NullTime) time.Time {
	return v.Time
}

func TimeToNullTime(v time.Time) sql.NullTime {
	return sql.NullTime{Valid: true, Time: v}
}

func Int64ToNullInt64(v int64) sql.NullInt64 {
	return sql.NullInt64{
		Int64: v,
		Valid: true,
	}
}

func NullInt64ToInt64(v sql.NullInt64) int64 {
	return v.Int64
}
