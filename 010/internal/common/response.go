package common

import (
	"github.com/go-chi/render"
	"net/http"
)

type ErrResponse struct {
	Error struct {
		Status  int    `json:"status"`
		Message string `json:"message"`
	} `json:"error"`
}

func (e *ErrResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.Error.Status)
	return nil
}

var _ render.Renderer = (*ErrResponse)(nil)

func NewErrResponse(code int, message string) render.Renderer {
	e := &ErrResponse{}
	e.Error.Status = code
	e.Error.Message = message

	return e
}
