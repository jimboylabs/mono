package common

import (
	"context"
	"github.com/spf13/cast"
	"net/http"
)

const (
	ContextKeyCursor = "paginate.cursor"
	ContextKeyCount  = "paginate.count"
)

func Paginate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cursorStr := r.URL.Query().Get("cursor")
		countStr := r.URL.Query().Get("count")
		count := cast.ToInt(countStr)

		if count == 0 {
			count = 10
		}

		ctx := context.WithValue(r.Context(), ContextKeyCursor, cursorStr)
		ctx = context.WithValue(ctx, ContextKeyCount, count)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
