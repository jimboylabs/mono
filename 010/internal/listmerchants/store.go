package listmerchants

import (
	"context"
	"database/sql"
	"github.com/spf13/cast"
	"ifood/internal/common"
	"ifood/internal/model/mysql"
	"log"

	_ "github.com/spf13/cast"
)

type Store interface {
	Query(ctx context.Context, request *ListMerchantsRequest) (*ListMerchantsResponse, error)
}

var _ Store = (*MySQLStore)(nil)

type MySQLStore struct {
	errorLog *log.Logger
	db       *sql.DB
}

func NewMySQLStore(errorLog *log.Logger, db *sql.DB) *MySQLStore {
	return &MySQLStore{errorLog: errorLog, db: db}
}

func (s *MySQLStore) toListMerchants(merchants []*mysql.Merchant) []*Merchant {
	var result []*Merchant

	for _, m := range merchants {
		result = append(result, &Merchant{
			ID:                 cast.ToString(m.ID),
			Name:               m.Name,
			Address1:           common.NullStringToString(m.Address1),
			Address2:           common.NullStringToString(m.Address2),
			AdministrativeArea: common.NullStringToString(m.AdministrativeArea),
		})
	}

	return result
}

func (s *MySQLStore) Query(ctx context.Context, request *ListMerchantsRequest) (*ListMerchantsResponse, error) {
	data := struct {
		Limit  int
		Cursor int
	}{
		Limit:  request.Count,
		Cursor: cast.ToInt(request.Cursor),
	}

	query := `
	SELECT
		id
		, name
		, address1
		, address2
		, administrative_area
	FROM merchants WHERE id > ? ORDER BY created_at ASC LIMIT ?`

	rows, err := s.db.QueryContext(ctx, query, data.Cursor, data.Limit)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var merchants []*mysql.Merchant
	var nextCursor uint64

	for rows.Next() {
		var merchant mysql.Merchant

		err := rows.Scan(&merchant.ID, &merchant.Name, &merchant.Address1, &merchant.Address2, &merchant.AdministrativeArea)
		if err != nil {
			s.errorLog.Println(err.Error())
			continue
		}

		merchants = append(merchants, &merchant)
		nextCursor = merchant.ID
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return &ListMerchantsResponse{
		Merchants:  s.toListMerchants(merchants),
		NextCursor: cast.ToString(nextCursor),
	}, nil
}
