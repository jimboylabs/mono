package listmerchants

import (
	"github.com/go-chi/render"
	"net/http"
)

type Merchant struct {
	ID                 string `json:"id"`
	Name               string `json:"name"`
	Address1           string `json:"address_1"`
	Address2           string `json:"address_2"`
	AdministrativeArea string `json:"administrative_area"`
}

type ListMerchantsRequest struct {
	Count  int
	Cursor string
}

var _ render.Renderer = (*ListMerchantsResponse)(nil)

type ListMerchantsResponse struct {
	Merchants  []*Merchant `json:"merchants"`
	NextCursor string      `json:"next_cursor"`
}

func (l *ListMerchantsResponse) Render(w http.ResponseWriter, r *http.Request) error {
	if l.Merchants == nil {
		l.Merchants = []*Merchant{}
	}

	return nil
}

