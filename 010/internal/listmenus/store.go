package listmenus

import (
	"context"
	"database/sql"
	"github.com/spf13/cast"
	"ifood/internal/common"
	"ifood/internal/model/mysql"
	"log"
)

var _ Store = (*MySQLStore)(nil)

type Store interface {
	Query(ctx context.Context, request *ListMenusRequest) (*ListMenusResponse, error)
}

type MySQLStore struct {
	errorLog *log.Logger

	db *sql.DB
}

func NewMySQLStore(errorLog *log.Logger, db *sql.DB) *MySQLStore {
	return &MySQLStore{errorLog: errorLog, db: db}
}

func (s *MySQLStore) toListMenus(menus []*mysql.Menu) []*Menu {
	var result []*Menu

	for _, m := range menus {
		result = append(result, &Menu{
			ID:          cast.ToString(m.ID),
			Name:        m.Name,
			MerchantID:  cast.ToString(m.MerchantID),
			Description: common.NullStringToString(m.Description),
			Price:       cast.ToInt(m.Price),
		})
	}

	return result
}

func (s *MySQLStore) Query(ctx context.Context, request *ListMenusRequest) (*ListMenusResponse, error) {
	data := struct {
		Limit  int
		Cursor int
	}{
		Limit:  request.Count,
		Cursor: cast.ToInt(request.Cursor),
	}

	query := `
	SELECT id
		 , name
		 , merchant_id
		 , description
		 , price
	FROM menus
	WHERE id > ?
	ORDER BY created_at ASC
	LIMIT ?`

	rows, err := s.db.QueryContext(ctx, query, data.Cursor, data.Limit)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var menus []*mysql.Menu
	var nextCursor uint64

	for rows.Next() {
		var m mysql.Menu
		err := rows.Scan(&m.ID, &m.Name, &m.MerchantID, &m.Description, &m.Price)
		if err != nil {
			s.errorLog.Println(err.Error())
			continue
		}

		menus = append(menus, &m)
		nextCursor = m.ID
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	resp := &ListMenusResponse{
		Menus:      s.toListMenus(menus),
		NextCursor: cast.ToString(nextCursor),
	}

	return resp, nil
}
