package listmenus

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"ifood/internal/common"
	"log"
	"net/http"
)

type Endpoint struct {
	infoLog  *log.Logger
	errorLog *log.Logger

	store Store
}

func NewEndpoint(infoLog, errorLog *log.Logger, store Store) *Endpoint {
	return &Endpoint{
		infoLog:  infoLog,
		errorLog: errorLog,
		store:    store,
	}
}

func SetupRoutes(r chi.Router, e *Endpoint) {
	r.With(common.Paginate).Get("/list-menus", e.handle)
}

func (e *Endpoint) handle(w http.ResponseWriter, r *http.Request) {

	request := &ListMenusRequest{}

	cursor, _ := r.Context().Value(common.ContextKeyCursor).(string)
	count, _ := r.Context().Value(common.ContextKeyCount).(int)

	request.Cursor = cursor
	request.Count = count

	response, err := e.store.Query(r.Context(), request)
	if err != nil {
		render.Status(r, http.StatusInternalServerError)
		render.JSON(w, r, common.NewErrResponse(http.StatusInternalServerError, err.Error()))
		return
	}

	render.Status(r, http.StatusOK)
	render.Render(w, r, response)
	return
}
