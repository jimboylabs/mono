package listmenus

import (
	"github.com/go-chi/render"
	"net/http"
)

type Menu struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	MerchantID  string `json:"merchant_id"`
	Description string `json:"description"`
	Price       int    `json:"price"`
}

type ListMenusRequest struct {
	Count  int
	Cursor string
}

var _ render.Renderer = (*ListMenusResponse)(nil)

type ListMenusResponse struct {
	Menus      []*Menu `json:"menus"`
	NextCursor string  `json:"next_cursor"`
}

func (l *ListMenusResponse) Render(w http.ResponseWriter, r *http.Request) error {
	if l.Menus == nil {
		l.Menus = []*Menu{}
	}

	return nil
}
