package createmerchant

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"ifood/internal/common"
	"ifood/internal/services"
	"log"
	"net/http"
)

type Endpoint struct {
	infoLog  *log.Logger
	errorLog *log.Logger

	store      Store
	taskClient *services.TaskClient
}

func NewEndpoint(infoLog, errorLog *log.Logger, store Store, taskClient *services.TaskClient) *Endpoint {
	return &Endpoint{infoLog: infoLog, errorLog: errorLog, store: store, taskClient: taskClient}
}

func SetupRoutes(r chi.Router, e *Endpoint) {
	r.Post("/create-merchant", e.handle)
}

func (e *Endpoint) handle(w http.ResponseWriter, r *http.Request) {
	request := &CreateMerchantRequest{}

	if err := render.Bind(r, request); err != nil {
		e.errorLog.Println(err.Error())
		render.Status(r, http.StatusBadRequest)
		render.JSON(w, r, common.NewErrResponse(http.StatusBadRequest, err.Error()))
		return
	}

	onCreate := func(merchantID string) {
		err := e.taskClient.New("merchant_create").Payload(merchantID).Save()
		if err != nil {
			e.errorLog.Println(err.Error())
		}
	}

	response, err := e.store.GetOrCreate(r.Context(), request, onCreate)
	if err != nil {
		e.errorLog.Println(err.Error())
		render.Status(r, http.StatusInternalServerError)
		render.JSON(w, r, common.NewErrResponse(http.StatusInternalServerError, err.Error()))
		return
	}

	render.Status(r, http.StatusCreated)
	render.JSON(w, r, response)
	return
}
