package createmerchant

import (
	"context"
	"database/sql"
	"github.com/spf13/cast"
	"ifood/internal/common"
	"ifood/internal/model/mysql"
)

type Store interface {
	GetOrCreate(ctx context.Context, request *CreateMerchantRequest, onCreate func(merchantID string)) (*CreateMerchantResponse, error)
}

var _ Store = (*MySQLStore)(nil)

type MySQLStore struct {
	db *sql.DB
}

func NewMySQLStore(db *sql.DB) *MySQLStore {
	s := &MySQLStore{db: db}
	return s
}

func (s *MySQLStore) getMerchantByID(ctx context.Context, ID string) (*CreateMerchantResponse, error) {
	merchant, err := mysql.MerchantByID(ctx, s.db, cast.ToUint64(ID))
	if err != nil {
		return nil, err
	}

	return &CreateMerchantResponse{
		CreateMerchantRequest: CreateMerchantRequest{
			ID:                 ID,
			Name:               merchant.Name,
			Address1:           common.NullStringToString(merchant.Address1),
			Address2:           common.NullStringToString(merchant.Address2),
			AdministrativeArea: common.NullStringToString(merchant.AdministrativeArea),
		},
	}, nil
}

func (s *MySQLStore) GetOrCreate(ctx context.Context, request *CreateMerchantRequest, onCreate func(merchantID string)) (*CreateMerchantResponse, error) {
	isCreate := true

	if request.ID != "" {
		isCreate = false
		return s.getMerchantByID(ctx, request.ID)
	}

	merchant := mysql.Merchant{
		Name:               request.Name,
		Address1:           common.StringToNullString(request.Address1),
		Address2:           common.StringToNullString(request.Address2),
		AdministrativeArea: common.StringToNullString(request.AdministrativeArea),
	}
	if err := merchant.Save(ctx, s.db); err != nil {
		return nil, err
	}

	response := &CreateMerchantResponse{
		CreateMerchantRequest: *request,
	}
	response.ID = cast.ToString(merchant.ID)

	if isCreate {
		onCreate(response.ID)
	}

	return response, nil
}
