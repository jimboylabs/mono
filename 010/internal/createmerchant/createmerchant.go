package createmerchant

import (
	"errors"
	"github.com/go-chi/render"
	"net/http"
)

var _ render.Binder = (*CreateMerchantRequest)(nil)

type CreateMerchantRequest struct {
	ID                 string `json:"id,omitempty"`
	Name               string `json:"name,omitempty"`
	Address1           string `json:"address_1,omitempty"`
	Address2           string `json:"address_2,omitempty"`
	AdministrativeArea string `json:"administrative_area,omitempty"`
}

func (c *CreateMerchantRequest) Bind(r *http.Request) error {
	if c.ID == "" {
		if c.Name == "" {
			return errors.New("missing required name field.")
		}

		if c.Address1 == "" {
			return errors.New("missing required address field.")
		}

		if c.AdministrativeArea == "" {
			return errors.New("missing required administrative area field.")
		}
	}

	return nil
}

type CreateMerchantResponse struct {
	CreateMerchantRequest
}
