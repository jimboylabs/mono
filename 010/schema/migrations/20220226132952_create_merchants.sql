-- migrate:up
create table merchants
(
    id                  bigint unsigned auto_increment primary key not null,
    name                varchar(45) not null,
    address1            varchar(100) null,
    address2            varchar(100) null,
    administrative_area varchar(50) null,

    created_at          timestamp   not null default current_timestamp,
    updated_at          timestamp null on update current_timestamp
);

-- migrate:down

drop table merchants;