-- migrate:up
create table menus
(
    id          bigint unsigned auto_increment primary key not null,
    name        varchar(45)                                not null,
    merchant_id bigint unsigned                            not null,
    description text                                       null,
    price       int(11)                                             default null,

    created_at  timestamp                                  not null default current_timestamp,
    updated_at  timestamp                                  null on update current_timestamp,

    constraint menus_merchant_id_foreign foreign key (merchant_id) references merchants (id)
);

-- migrate:down
alter table menus
    drop foreign key menus_merchant_id_foreign;

drop table menus;

