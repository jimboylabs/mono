-- migrate:up
create table users (
    id integer not null primary key autoincrement,
    name varchar(45) not null,
    email varchar(45) not null,
    phone varchar(45) null,
    hashed_password varchar(128) null,

    created_at datetime not null default current_timestamp,
    updated_at datetime null
);

create trigger users_trig after update on users
begin
    update users set updated_at = datetime('now') where id = NEW.id;
end;

-- migrate:down

drop trigger if exists users_trig;

drop table users;

