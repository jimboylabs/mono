package minipos

import (
	"context"
	"fmt"
	"minipos"
	"minipos/config"
)

func Run(ctx context.Context, logger minipos.Logger) (err error) {
	cfg := config.New()
	err = cfg.Validate()
	if err != nil {
		err = fmt.Errorf("failed tp validate configuration: %w", err)
		return
	}

	return nil
}
