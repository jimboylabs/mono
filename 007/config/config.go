package config

import (
	"github.com/go-playground/validator/v10"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

const (
	DefaultHost          = ":3000"
	DefaultSQLiteAddress = "sqlite:database.sqlite3"
	DefaultRedisAddr     = "localhost:6379"
)

type Validator interface {
	Struct(interface{}) error
}

type Configuration struct {
	validator Validator `validate:"required"`

	Host string `validate:"required"`

	SQLiteAddr string `validate:"required"`

	RedisAddr     string `validate:"required"`
	RedisUsername string `validate:"required"`
	RedisPassword string `validate:"required"`
}

func init() {
	viper.SetEnvPrefix("MINIPOS")
	viper.AutomaticEnv()

	handleError(viper.BindEnv("host"))
	handleError(viper.BindEnv("sqlite_addr"))
	handleError(viper.BindEnv("redis_addr"))
	handleError(viper.BindEnv("redis_user"))
	handleError(viper.BindEnv("redis_password"))

	viper.SetDefault("host", DefaultHost)
	viper.SetDefault("sqlite_addr", DefaultSQLiteAddress)
	viper.SetDefault("redis_addr", DefaultRedisAddr)
}

func New() (c *Configuration) {
	c = &Configuration{}
	c.validator = validator.New()

	c.Host = viper.GetString("host")

	c.SQLiteAddr = viper.GetString("sqlite_addr")

	c.RedisAddr = viper.GetString("redis_addr")

	return
}

func handleError(err error) {
	if err != nil {
		log.Fatalf("Failred to process the configuration: %v", err)
	}
}

func (c *Configuration) Validate() (err error) {
	return nil
}
