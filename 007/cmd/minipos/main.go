package main

import (
	"context"
	log "github.com/sirupsen/logrus"
	"minipos/app/minipos"
)

func main() {
	logger := log.New()

	logger.Infoln("Starting server")

	err := minipos.Run(context.Background(), logger)
	if err != nil {
		logger.Infoln("Server shutting down")
	}
}
