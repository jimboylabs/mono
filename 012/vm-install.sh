nix-env -iA \
	nixpkgs.direnv \
	nixpkgs.ripgrep \
	nixpkgs.bat \
	nixpkgs.tmux \
	nixpkgs.delta \
	nixpkgs.chezmoi \
    nixpkgs.fzf \
    nixpkgs.zoxide \
    nixpkgs.fd

curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
