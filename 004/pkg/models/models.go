package models

type Menu struct {
	ID uint

	Name        string
	Category    string
	Merchant    string
	Description string

	Price int64
}
