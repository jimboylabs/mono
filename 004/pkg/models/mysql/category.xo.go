package mysql

// Code generated by xo. DO NOT EDIT.

import (
	"context"
	"database/sql"
)

// Category represents a row from 'gofood.categories'.
type Category struct {
	ID        uint         `json:"id"`         // id
	Name      string       `json:"name"`       // name
	CreatedAt sql.NullTime `json:"created_at"` // created_at
	UpdatedAt sql.NullTime `json:"updated_at"` // updated_at
	// xo fields
	_exists, _deleted bool
}

// Exists returns true when the Category exists in the database.
func (c *Category) Exists() bool {
	return c._exists
}

// Deleted returns true when the Category has been marked for deletion from
// the database.
func (c *Category) Deleted() bool {
	return c._deleted
}

// Insert inserts the Category to the database.
func (c *Category) Insert(ctx context.Context, db DB) error {
	switch {
	case c._exists: // already exists
		return logerror(&ErrInsertFailed{ErrAlreadyExists})
	case c._deleted: // deleted
		return logerror(&ErrInsertFailed{ErrMarkedForDeletion})
	}
	// insert (primary key generated and returned by database)
	const sqlstr = `INSERT INTO gofood.categories (` +
		`name, created_at, updated_at` +
		`) VALUES (` +
		`?, ?, ?` +
		`)`
	// run
	logf(sqlstr, c.Name, c.CreatedAt, c.UpdatedAt)
	res, err := db.ExecContext(ctx, sqlstr, c.Name, c.CreatedAt, c.UpdatedAt)
	if err != nil {
		return logerror(err)
	}
	// retrieve id
	id, err := res.LastInsertId()
	if err != nil {
		return logerror(err)
	} // set primary key
	c.ID = uint(id)
	// set exists
	c._exists = true
	return nil
}

// Update updates a Category in the database.
func (c *Category) Update(ctx context.Context, db DB) error {
	switch {
	case !c._exists: // doesn't exist
		return logerror(&ErrUpdateFailed{ErrDoesNotExist})
	case c._deleted: // deleted
		return logerror(&ErrUpdateFailed{ErrMarkedForDeletion})
	}
	// update with primary key
	const sqlstr = `UPDATE gofood.categories SET ` +
		`name = ?, created_at = ?, updated_at = ? ` +
		`WHERE id = ?`
	// run
	logf(sqlstr, c.Name, c.CreatedAt, c.UpdatedAt, c.ID)
	if _, err := db.ExecContext(ctx, sqlstr, c.Name, c.CreatedAt, c.UpdatedAt, c.ID); err != nil {
		return logerror(err)
	}
	return nil
}

// Save saves the Category to the database.
func (c *Category) Save(ctx context.Context, db DB) error {
	if c.Exists() {
		return c.Update(ctx, db)
	}
	return c.Insert(ctx, db)
}

// Upsert performs an upsert for Category.
func (c *Category) Upsert(ctx context.Context, db DB) error {
	switch {
	case c._deleted: // deleted
		return logerror(&ErrUpsertFailed{ErrMarkedForDeletion})
	}
	// upsert
	const sqlstr = `INSERT INTO gofood.categories (` +
		`id, name, created_at, updated_at` +
		`) VALUES (` +
		`?, ?, ?, ?` +
		`)` +
		` ON DUPLICATE KEY UPDATE ` +
		`name = VALUES(name), created_at = VALUES(created_at), updated_at = VALUES(updated_at)`
	// run
	logf(sqlstr, c.ID, c.Name, c.CreatedAt, c.UpdatedAt)
	if _, err := db.ExecContext(ctx, sqlstr, c.ID, c.Name, c.CreatedAt, c.UpdatedAt); err != nil {
		return logerror(err)
	}
	// set exists
	c._exists = true
	return nil
}

// Delete deletes the Category from the database.
func (c *Category) Delete(ctx context.Context, db DB) error {
	switch {
	case !c._exists: // doesn't exist
		return nil
	case c._deleted: // deleted
		return nil
	}
	// delete with single primary key
	const sqlstr = `DELETE FROM gofood.categories ` +
		`WHERE id = ?`
	// run
	logf(sqlstr, c.ID)
	if _, err := db.ExecContext(ctx, sqlstr, c.ID); err != nil {
		return logerror(err)
	}
	// set deleted
	c._deleted = true
	return nil
}

// CategoryByID retrieves a row from 'gofood.categories' as a Category.
//
// Generated from index 'categories_id_pkey'.
func CategoryByID(ctx context.Context, db DB, id uint) (*Category, error) {
	// query
	const sqlstr = `SELECT ` +
		`id, name, created_at, updated_at ` +
		`FROM gofood.categories ` +
		`WHERE id = ?`
	// run
	logf(sqlstr, id)
	c := Category{
		_exists: true,
	}
	if err := db.QueryRowContext(ctx, sqlstr, id).Scan(&c.ID, &c.Name, &c.CreatedAt, &c.UpdatedAt); err != nil {
		return nil, logerror(err)
	}
	return &c, nil
}
