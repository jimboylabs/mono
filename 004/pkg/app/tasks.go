package app

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/hibiken/asynq"
	"github.com/spf13/cast"
	"gitlab.com/jimboylabs/gofoodapi/pkg/models"
	"gitlab.com/jimboylabs/gofoodapi/pkg/models/mysql"
	"gitlab.com/jimboylabs/gofoodapi/pkg/utils"
)

const TypeMenuReindex = "menu:reindex"

type MenuReindexPayload struct {
	MenuID uint
}

func NewMenuReindexTask(menuID uint) (*asynq.Task, error) {
	payload, err := json.Marshal(MenuReindexPayload{MenuID: menuID})
	if err != nil {
		return nil, err
	}

	return asynq.NewTask(TypeMenuReindex, payload), nil
}

var _ asynq.Handler = (*MenuReindexProcessor)(nil)

type MenuReindexProcessor struct {
	App *Application
}

func NewMenuReindexProcessor(application *Application) *MenuReindexProcessor {
	return &MenuReindexProcessor{App: application}
}

func (m MenuReindexProcessor) ProcessTask(ctx context.Context, task *asynq.Task) error {
	var p MenuReindexPayload

	if err := json.Unmarshal(task.Payload(), &p); err != nil {
		return fmt.Errorf("json.Unmarshal failed: %v: %w", err, asynq.SkipRetry)
	}

	menu, err := mysql.MenuByID(ctx, m.App.DB, cast.ToUint(p.MenuID))
	if err != nil {
		return err
	}

	category, err := mysql.CategoryByID(ctx, m.App.DB, menu.CategoryID)
	if err != nil {
		return err
	}

	merchant, err := mysql.MerchantByID(ctx, m.App.DB, menu.MerchantID)
	if err != nil {
		return err
	}

	err = m.App.SearchEngine.IndexMenu(models.Menu{
		ID:          menu.ID,
		Name:        menu.Name,
		Category:    category.Name,
		Merchant:    merchant.Name,
		Description: utils.NullStringToString(menu.Description),
		Price:       menu.Price,
	})
	if err != nil {
		return err
	}

	return nil
}
