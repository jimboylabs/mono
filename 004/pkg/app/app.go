package app

import (
	"context"
	"database/sql"
	"github.com/bxcodec/faker/v3"
	"github.com/hibiken/asynq"
	"gitlab.com/jimboylabs/gofoodapi/pkg/models/mysql"
	"gitlab.com/jimboylabs/gofoodapi/pkg/searchengine"
	"gitlab.com/jimboylabs/gofoodapi/pkg/utils"
	"log"
	"net/http"
	"time"
)

type Application struct {
	ErrorLog *log.Logger
	InfoLog  *log.Logger

	DB *sql.DB

	AsynqClient *asynq.Client

	SearchEngine searchengine.SearchEngine
}

func (app *Application) Home(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello from gofood!"))
}

func (app *Application) CreateMenu(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.Header().Set("Allow", http.MethodPost)
		app.clientError(w, http.StatusMethodNotAllowed)
		return
	}

	now := time.Now()

	m := mysql.Menu{
		Name:        faker.Name(),
		CategoryID:  1,
		Price:       10000,
		Description: utils.StringToNullString(faker.Sentence()),
		MerchantID:  1,
		CreatedAt:   utils.TimeToNullTime(now),
	}

	ctx := context.Background()

	if err := m.Save(ctx, app.DB); err != nil {
		app.ErrorLog.Println(err)
		app.clientError(w, http.StatusInternalServerError)
		return
	}

	task, err := NewMenuReindexTask(m.ID)
	if err != nil {
		app.ErrorLog.Println(err)
	}
	_, err = app.AsynqClient.Enqueue(task, asynq.ProcessIn(20*time.Second))
	if err != nil {
		app.ErrorLog.Println(err)
	}
}
