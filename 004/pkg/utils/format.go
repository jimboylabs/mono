package utils

import (
	"database/sql"
	"time"
)

func StringToNullString(s string) sql.NullString {
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}

func NullStringToString(v sql.NullString) string {
	if !v.Valid {
		return ""
	}

	return v.String
}

func TimeToNullTime(t time.Time) sql.NullTime {
	return sql.NullTime{
		Time:  t,
		Valid: true,
	}
}
