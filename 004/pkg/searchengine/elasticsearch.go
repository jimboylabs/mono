package searchengine

import (
	"context"
	"fmt"
	"github.com/olivere/elastic"
	"gitlab.com/jimboylabs/gofoodapi/pkg/models"
)

const MenuIndex = "menu"

var _ SearchEngine = (*ElasticSearchEngine)(nil)

type ElasticSearchEngine struct {
	Client *elastic.Client
}

func (e ElasticSearchEngine) Init() error {
	exists, err := e.Client.IndexExists(MenuIndex).Do(context.Background())
	if err != nil {
		return err
	}

	if !exists {
		// Creating a new index
		mapping := `{
  "mappings": {
    "doc": {
      "properties": {
        "name": {
          "type": "text"
        },
        "category": {
          "type": "text"
        },
        "price": {
          "type": "integer"
        },
        "description": {
          "type": "text"
        },
        "merchant": {
          "type": "text"
        }
      }
    }
  }
}`

		createIndex, err := e.Client.CreateIndex(MenuIndex).Body(mapping).Do(context.Background())
		if err != nil {
			return err
		}

		if !createIndex.Acknowledged {
		}
	}

	return nil
}

func (e ElasticSearchEngine) IndexMenu(menu models.Menu) error {
	data := struct {
		Name        string `json:"name"`
		Description string `json:"description"`
		Category    string `json:"category"`
		Merchant    string `json:"merchant"`
		Price       int64  `json:"price"`
	}{
		Name:        menu.Name,
		Description: menu.Description,
		Category:    menu.Category,
		Merchant:    menu.Merchant,
		Price:       menu.Price,
	}

	_, err := e.Client.Index().Index(MenuIndex).
		Type("doc").
		Id(fmt.Sprintf("%d", menu.ID)).
		BodyJson(data).Do(context.Background())
	if err != nil {
		return err
	}

	return nil
}
