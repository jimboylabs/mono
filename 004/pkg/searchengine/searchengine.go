package searchengine

import "gitlab.com/jimboylabs/gofoodapi/pkg/models"

type SearchEngine interface {
	Init() error
	IndexMenu(menu models.Menu) error
}
