package main

import (
	"flag"
	_ "github.com/go-sql-driver/mysql"
	"github.com/hibiken/asynq"
	"github.com/olivere/elastic"
	"github.com/xo/dburl"
	"gitlab.com/jimboylabs/gofoodapi/pkg/app"
	"gitlab.com/jimboylabs/gofoodapi/pkg/searchengine"
	"log"
	"net/http"
	"os"
)

func main() {
	addr := flag.String("addr", ":4000", "HTTP network address")

	dsn := flag.String("dsn", "mysql://root:root@127.0.0.1/gofood?parseTime=true", "MySQL data source name")

	flag.Parse()

	infoLog := log.New(os.Stdout, "INFO\t", log.Ldate|log.Ltime)
	errorLog := log.New(os.Stdout, "ERROR\t", log.Ldate|log.Ltime|log.Lshortfile)

	db, err := dburl.Open(*dsn)
	if err != nil {
		errorLog.Fatal(err)
	}
	defer db.Close()

	client, err := elastic.NewClient(elastic.SetURL("http://127.0.0.1:9200"))
	if err != nil {
		errorLog.Fatal(err)
	}

	asynqClient := asynq.NewClient(asynq.RedisClientOpt{Addr: "127.0.0.1:6379"})
	defer asynqClient.Close()

	se := &searchengine.ElasticSearchEngine{Client: client}

	if err := se.Init(); err != nil {
		return
	}

	a := &app.Application{
		ErrorLog:     errorLog,
		InfoLog:      infoLog,
		DB:           db,
		AsynqClient:  asynqClient,
		SearchEngine: se,
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/", a.Home)
	mux.HandleFunc("/menu", a.CreateMenu)

	srv := &http.Server{
		Addr:     *addr,
		Handler:  mux,
		ErrorLog: errorLog,
	}

	infoLog.Printf("Starting server on %s", *addr)
	err = srv.ListenAndServe()
	errorLog.Fatal(err)
}
