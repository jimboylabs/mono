# Simple Full-Text Search Engine

### Tests result

- session1 (without regex but the search is case sensitive)
    - term= catterpilar
    - Load documents took 24.307590915s
    - Search documents took 54.433101ms
    - total document: 20s
- session2 (with regex)
    - term = catterpillar
    - Load documents took 24.636869833s
    - Search documents took 1.562396296s
    - total document: 18
- session3 (with index)
    - term = catterpillar
    - Build index took 1.093739ms
    - Search against the index took 4.86µs

### TODO

- Extend boolean queries to support OR and NOT.
- Store the index on disk:
- Rebuilding the index on every application restart may take a while.
- Large indexes may not fit in memory.
- Experiment with memory and CPU-efficient data formats for storing sets of document IDs. Take a look at Roaring Bitmaps.
- Support indexing multiple document fields.
- Sort results by relevance.

### Refs
- https://artem.krylysov.com/blog/2020/07/28/lets-build-a-full-text-search-engine/
- https://github.com/akrylysov/simplefts