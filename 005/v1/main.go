package main

import (
	"encoding/xml"
	"flag"
	"fmt"
	"log"
	"os"
	"time"
)

type config struct {
	path string
	term string
}

func loadDocuments(path string) ([]Document, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	decoder := xml.NewDecoder(f)
	dump := struct {
		Documents []Document `xml:"doc"`
	}{}

	if err := decoder.Decode(&dump); err != nil {
		return nil, err
	}

	documents := dump.Documents
	for i := range documents {
		documents[i].ID = i
	}

	return documents, nil
}

func run(cfg config) error {
	search := SimpleSearchEngine{}
	if err := search.Open(); err != nil {
		return err
	}
	defer search.Close()

	if !search.IndexExists() {
		docs, err := loadDocuments(cfg.path)
		if err != nil {
			return err
		}
		search.AddDocuments(docs)
	}

	start := time.Now()

	result := search.Search(cfg.term)

	for _, r := range result {
		fmt.Println(r)
	}

	elapsed := time.Since(start)
	fmt.Printf("Search against the index took %s\n", elapsed)

	return nil
}

func main() {
	var path string
	var term string

	flag.StringVar(&path, "path", "/home/jimbo/Downloads/enwiki-latest-abstract1.xml", "path to the xml file")
	flag.StringVar(&term, "term", "", "what are you looking for?")

	flag.Parse()

	cfg := config{
		path: path,
		term: term,
	}

	if err := run(cfg); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}
