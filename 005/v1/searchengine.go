package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type SearchEngine interface {
	Open() error
	AddDocuments(docs []Document)
	IndexExists() bool
	Search(term string) [][]int
	Close() error
}

type Document struct {
	Title string `xml:"title"`
	URL   string `xml:"url"`
	Text  string `xml:"abstract"`
	ID    int    `xml:"id"`
}

type index map[string][]int

var _ SearchEngine = (*SimpleSearchEngine)(nil)

type SimpleSearchEngine struct {
	docs map[int]Document
	idx  index
}

func (s *SimpleSearchEngine) IndexExists() bool {
	return len(s.idx) > 0
}

func (s *SimpleSearchEngine) Open() error {
	s.init()

	file, err := ioutil.ReadFile("idx.json")
	if err != nil {
		return err
	}

	err = json.Unmarshal(file, &s.idx)
	if err != nil {
		return err
	}

	return nil
}

func (s *SimpleSearchEngine) Search(term string) [][]int {
	var result [][]int

	t := AnalyzeText(term)

	for _, token := range t.Analyze() {
		if ids, ok := s.idx[token]; ok {
			result = append(result, ids)
		}
	}

	return result
}

func (s *SimpleSearchEngine) init() {
	s.idx = index{}
	s.docs = map[int]Document{}
}

func (s *SimpleSearchEngine) AddDocuments(docs []Document) {
	if s.idx == nil {
		return
	}

	if s.docs == nil {
		return
	}

	for _, doc := range docs {

		t := AnalyzeText(doc.Text)
		for _, token := range t.Analyze() {
			ids := s.idx[token]

			if ids != nil && ids[len(ids)-1] == doc.ID {
				// Don't add the same ID twice
				continue
			}

			s.idx[token] = append(ids, doc.ID)
		}

		s.docs[doc.ID] = doc
	}
}

func (s *SimpleSearchEngine) Close() error {
	// write idx from memory to the file
	jsonStr, err := json.Marshal(s.idx)
	if err != nil {
		return err
	}

	return ioutil.WriteFile("idx.json", jsonStr, os.ModePerm)
}
