package main

import (
	"reflect"
	"regexp"
	"testing"
)

func TestDocument_Match(t *testing.T) {

	tests := []struct {
		Name string
		Term string
		Text string
		Want bool
	}{
		{
			Name: "caterpillar",
			Term: "caterpillar",
			Text: "caterpillar",
			Want: true,
		},
		{
			Name: "Caterpillar",
			Term: "caterpillar",
			Text: "Caterpillar",
			Want: true,
		},
		{
			Name: "Pig",
			Term: "caterpillar",
			Text: "Pig",
			Want: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			re := regexp.MustCompile(`(?i)\b` + tt.Term + `\b`)

			document := Document{
				Text: tt.Text,
			}

			got := document.Match(re)

			if got != tt.Want {
				t.Errorf("got %t want %t", got, tt.Want)
			}
		})
	}
}

func TestDocument_Tokenize(t *testing.T) {
	tests := []struct {
		Name string
		Text string
		Want []string
	}{
		{
			Name: "donut",
			Text: "A donut on a glass plate. Only the donuts.",
			Want: []string{"A", "donut", "on", "a", "glass", "plate", "Only", "the", "donuts"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			d := Document{
				Text: tt.Text,
			}

			got := d.Tokenize()

			if !reflect.DeepEqual(tt.Want, got) {
				t.Errorf("got %v want %v", got, tt.Want)
			}
		})
	}
}

func TestDocument_Analize(t *testing.T) {
	tests := []struct {
		Text string
		Want []string
	}{
		{
			Text: "A donut on a glass plate. Only the donuts.",
			Want: []string{"donut", "on", "glass", "plate", "only", "donut"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.Text, func(t *testing.T) {
			d := Document{Text: tt.Text}

			got := d.Analize()

			if !reflect.DeepEqual(got, tt.Want) {
				t.Errorf("got %v want %v", got, tt.Want)
			}
		})
	}
}
