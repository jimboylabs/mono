package main

import (
	"encoding/xml"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
	"time"
	"unicode"

	snowballeng "github.com/kljensen/snowball/english"
)

type index map[string][]int

func (i index) add(docs []Document) {
	for _, doc := range docs {
		for _, token := range doc.Analize() {
			ids := i[token]
			if ids != nil && ids[len(ids)-1] == doc.ID {
				// Don't add the same ID twice
				continue
			}

			i[token] = append(ids, doc.ID)
		}
	}
}

func (i index) search(text string) [][]int {
	var r [][]int

	t := Document{
		Text: text,
	}

	for _, token := range t.Analize() {
		if ids, ok := i[token]; ok {
			r = append(r, ids)
		}
	}

	return r
}

type Document struct {
	Title string `xml:"title"`
	URL   string `xml:"url"`
	Text  string `xml:"abstract"`
	ID    int    `xml:"id"`
}

func (d Document) Match(re *regexp.Regexp) bool {
	return re.MatchString(d.Text)
}

func lowercaseFilter(tokens []string) []string {
	r := make([]string, len(tokens))
	for i, token := range tokens {
		r[i] = strings.ToLower(token)
	}
	return r
}

var stopwords = map[string]struct{}{
	"a":    {},
	"and":  {},
	"be":   {},
	"have": {},
	"i":    {},
	"in":   {},
	"of":   {},
	"that": {},
	"the":  {},
	"to":   {},
}

func stopwordFilter(tokens []string) []string {
	r := make([]string, 0, len(tokens))
	for _, token := range tokens {
		_, ok := stopwords[token]
		if !ok {
			r = append(r, token)
		}
	}
	return r
}

func stemmerFilter(tokens []string) []string {
	r := make([]string, len(tokens))
	for i, token := range tokens {
		r[i] = snowballeng.Stem(token, false)
	}
	return r
}

func (d Document) Tokenize() []string {
	return strings.FieldsFunc(d.Text, func(r rune) bool {
		// Split on any character that is not a letter or a number
		return !unicode.IsLetter(r) && !unicode.IsNumber(r)
	})
}

func (d Document) Analize() []string {
	tokens := d.Tokenize()

	tokens = lowercaseFilter(tokens)
	tokens = stopwordFilter(tokens)
	tokens = stemmerFilter(tokens)

	return tokens
}

func loadDocuments(path string) ([]Document, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	decoder := xml.NewDecoder(f)
	dump := struct {
		Documents []Document `xml:"doc"`
	}{}

	if err := decoder.Decode(&dump); err != nil {
		return nil, err
	}

	documents := dump.Documents
	for i := range documents {
		documents[i].ID = i
	}

	return documents, nil
}

func search(docs []Document, term string) []Document {
	re := regexp.MustCompile(`(?i)\b` + term + `\b`)

	var r []Document

	for _, doc := range docs {
		if re.MatchString(doc.Text) {
			r = append(r, doc)
		}
	}

	return r
}

type Config struct {
	Path string
	Term string
}

func run(config Config) error {

	start := time.Now()

	documents, err := loadDocuments(config.Path)
	if err != nil {
		return err
	}

	elapsed := time.Since(start)

	fmt.Printf("Load documents took %s\n", elapsed)

	start = time.Now()

	result := search(documents, config.Term)

	for _, r := range result {
		fmt.Println(r)
	}

	elapsed = time.Since(start)

	fmt.Printf("Search documents took %s\n", elapsed)

	fmt.Printf("total Document: %d\n", len(result))

	// build index
	start = time.Now()

	idx := index{}

	idx.add(result)

	elapsed = time.Since(start)
	fmt.Printf("Build index took %s\n", elapsed)

	start = time.Now()

	searchResult := idx.search(config.Term)

	elapsed = time.Since(start)
	fmt.Printf("Search against the index took %s\n", elapsed)

	fmt.Println(searchResult)

	return nil
}

func main() {
	var path string
	var term string

	flag.StringVar(&path, "path", "/home/jimbo/Downloads/enwiki-latest-abstract1.xml", "path to the xml file")
	flag.StringVar(&term, "term", "", "what are you looking for?")

	flag.Parse()

	config := Config{
		Path: path,
		Term: term,
	}

	if err := run(config); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}
