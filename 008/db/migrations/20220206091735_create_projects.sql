-- migrate:up

create table projects(
    id int(11) auto_increment primary key not null,
    name varchar(45) not null,
    description varchar(255) null,
    created_at timestamp not null default current_timestamp,
    updated_at timestamp null on update current_timestamp
);

-- migrate:down

drop table projects;