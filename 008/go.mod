module gotrans

go 1.17

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/go-sql-driver/mysql v1.6.0
	github.com/xo/dburl v0.9.0
)

require github.com/spf13/cast v1.4.1 // indirect
