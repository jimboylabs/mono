import { IResourceComponentsProps, List, Table, useTable } from "@pankod/refine";

import { IProject } from "interfaces";


export const ProjectList: React.FC<IResourceComponentsProps> = () => {
    const { tableProps } = useTable<IProject>();

    return (
        <List>
            <Table {...tableProps} rowKey={"id"}>
                <Table.Column dataIndex={"name"} title={"name"} />
                <Table.Column dataIndex={"description"} title={"description"} />
            </Table>
        </List>
    );
};
