import { Refine } from "@pankod/refine";
import routerProvider from "@pankod/refine-react-router";

import "@pankod/refine/dist/styles.min.css";
import simpleRestDataProvider from "@pankod/refine-simple-rest";
import { authProvider } from "authProvider";
import { PostCreate, PostEdit, PostList, PostShow } from "pages/posts";
import { ProjectList } from "pages/projects";

function App() {
    const API_URL = "https://api.fake-rest.refine.dev";
    const dataProvider = simpleRestDataProvider(API_URL);
    return (
        <Refine
            routerProvider={routerProvider}
            dataProvider={dataProvider}
            authProvider={authProvider}
            resources={[
                {
                    name: "posts",
                    list: PostList,
                    create: PostCreate,
                    edit: PostEdit,
                    show: PostShow
                },
                {
                    name: "projects",
                    list: ProjectList
                }
            ]}
        ></Refine>
    );
}

export default App;
