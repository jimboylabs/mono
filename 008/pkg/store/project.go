package store

import (
	"context"
	"database/sql"
	"github.com/spf13/cast"
	"gotrans/pkg/helper"
	"gotrans/pkg/model/mysql"
)

type MysqlProjectStore struct {
	db *sql.DB
}

func (s *MysqlProjectStore) Update(ctx context.Context, project Project) error {
	p, err := mysql.ProjectByID(ctx, s.db, cast.ToInt(project.ID))
	if err != nil {
		return err
	}

	p.Name = project.Name
	p.Description = helper.StringToNullString(project.Description)

	err = p.Update(ctx, s.db)
	if err != nil {
		return err
	}

	return nil
}

func (s *MysqlProjectStore) List(ctx context.Context, options ProjectListOptions) ([]Project, error) {
	selectQuery := `SELECT id, name, description FROM projects WHERE 1=1 ORDER BY id DESC`

	var params []interface{}

	params = []interface{}{}

	if options.Limit > 0 {
		selectQuery += `LIMIT ?, ?`
		params = append(params, options.Offset, options.Limit)
	}

	rows, err := s.db.QueryContext(ctx, selectQuery, params...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var projects []Project

	for rows.Next() {
		var p mysql.Project

		err := rows.Scan(&p.ID, &p.Name, &p.Description)
		if err != nil {
			return nil, err
		}

		projects = append(projects, Project{
			ID:          cast.ToString(p.ID),
			Name:        p.Name,
			Description: helper.NullStringToString(p.Description),
		})
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return projects, nil
}

func NewMysqlProjectStore(db *sql.DB) *MysqlProjectStore {
	return &MysqlProjectStore{db: db}
}

func (s *MysqlProjectStore) GetByID(ctx context.Context, projectID int) (Project, error) {
	p, err := mysql.ProjectByID(ctx, s.db, projectID)
	if err != nil {
		return Project{}, err
	}

	return Project{
		ID:          cast.ToString(p.ID),
		Name:        p.Name,
		Description: helper.NullStringToString(p.Description),
	}, nil
}

func (s *MysqlProjectStore) Create(ctx context.Context, project *Project) error {
	p := mysql.Project{
		Name:        project.Name,
		Description: helper.StringToNullString(project.Description),
	}

	err := p.Save(ctx, s.db)
	if err != nil {
		return err
	}

	project.ID = cast.ToString(p.ID)

	return nil
}

var _ ProjectStore = (*MysqlProjectStore)(nil)
