package store

import (
	"context"
)

type Project struct {
	ID          string `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

type ProjectListOptions struct {
	Limit  int
	Offset int
}

type ProjectStore interface {
	List(ctx context.Context, options ProjectListOptions) ([]Project, error)
	Update(ctx context.Context, project Project) error
	GetByID(ctx context.Context, projectID int) (Project, error)
	Create(ctx context.Context, project *Project) error
}
