package helper

import (
	"database/sql"
	"time"
)

func NullStringToString(v sql.NullString) string {
	if !v.Valid {
		return ""
	}

	return v.String
}

func StringToNullString(v string) sql.NullString {
	return sql.NullString{Valid: true, String: v}
}

func NullTimeToTime(v sql.NullTime) time.Time {
	return v.Time
}

func TimeToNullTime(v time.Time) sql.NullTime {
	return sql.NullTime{Valid: true, Time: v}
}
