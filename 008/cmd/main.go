package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/go-chi/chi/v5"
	_ "github.com/go-sql-driver/mysql"
	"github.com/xo/dburl"
	"gotrans/pkg/store"
	"log"
	"net/http"
	"os"
)

func run() error {
	dsn := flag.String("dsn", "mysql://root:root@localhost:3306/gotrans?parseTime=true", "mysql data source name")

	flag.Parse()

	db, err := dburl.Open(*dsn)
	if err != nil {
		return err
	}

	projectStore := store.NewMysqlProjectStore(db)

	seed := false

	if seed {
		project := &store.Project{
			Name:        "Go Trans",
			Description: "A description",
		}

		err = projectStore.Create(context.Background(), project)
		if err != nil {
			return err
		}

		fmt.Println("project id", project.ID)

		projects, err := projectStore.List(context.Background(), store.ProjectListOptions{
			Limit:  10,
			Offset: 0,
		})
		if err != nil {
			return err
		}

		for _, project := range projects {
			fmt.Println("id", project.ID, "name", project.Name)
		}
	}

	webserver := true

	if webserver {
		r := chi.NewRouter()

		projectServer := NewProjectServer(projectStore)

		r.Mount("/projects", projectServer.Routes())

		addr := ":5000"

		fmt.Printf("server is running on %s\n", addr)

		err := http.ListenAndServe(addr, r)
		if err != nil {
			return err
		}
	}

	return nil
}

func main() {
	if err := run(); err != nil {
		log.Fatal(err)
		os.Exit(1)
	}
}
