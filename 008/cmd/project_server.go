package main

import (
	"context"
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"github.com/spf13/cast"
	"gotrans/pkg/store"
	"log"
	"net/http"
)

type ProjectServer struct {
	store store.ProjectStore
}

func NewProjectServer(projectStore store.ProjectStore) *ProjectServer {
	return &ProjectServer{store: projectStore}
}

func (s *ProjectServer) Routes() chi.Router {
	r := chi.NewRouter()

	r.Get("/", s.list)
	r.Get("/{id}", s.detail)

	return r
}

func (s *ProjectServer) detail(w http.ResponseWriter, r *http.Request) {
	projectID := chi.URLParam(r, "id")

	project, err := s.store.GetByID(context.Background(), cast.ToInt(projectID))
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(project)
	w.WriteHeader(http.StatusOK)
}

func (s *ProjectServer) list(w http.ResponseWriter, r *http.Request) {
	projects, err := s.store.List(context.Background(), store.ProjectListOptions{})
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(projects)
	w.WriteHeader(http.StatusOK)
}
