package main

import (
	"errors"
	"fmt"
	"github.com/ardanlabs/conf/v3"
	"go.uber.org/zap"
	"mimiko/app/services/mimikoapi/handlers"
	"mimiko/business/sys/database"
	"mimiko/business/web/auth"
	"mimiko/foundation/logger"
	"net/http"
	"os"
)

func main() {
	// Construct the application logger.
	log, err := logger.New("MIMIKO-API")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer log.Sync()

	if err := run(log); err != nil {
		log.Errorw("startup", "ERROR", err)
		log.Sync()
		os.Exit(1)
	}
}

var build = "develop"

func run(log *zap.SugaredLogger) error {
	// Configuration
	config := struct {
		conf.Version
		Web struct {
			APIHost string `conf:"localhost:8080,env:API_HOST,default:0.0.0.0:8080"`
		}
		DB struct {
			MySQLAddr string `conf:"env:MYSQL_ADDR,default:mysql://mimiko:mimiko@192.168.8.212/mimiko?parseTime=true"`
		}
	}{
		Version: conf.Version{
			Build: build,
			Desc:  "copyright information here",
		},
	}

	const prefix = "MIMIKO"

	help, err := conf.Parse(prefix, &config)
	if err != nil {
		if errors.Is(err, conf.ErrHelpWanted) {
			fmt.Println(help)
			return nil
		}

		return fmt.Errorf("parsing config: %w", err)
	}

	log.Infow("startup", "status", "initializing authentication support")

	auth, err := auth.New([]byte("secret"))
	if err != nil {
		return fmt.Errorf("constructing auth: %w", err)
	}

	// Database support

	db, err := database.Open(database.Config{URL: config.DB.MySQLAddr})
	if err != nil {
		return fmt.Errorf("connecting to db: %w", err)
	}
	defer func() {
		log.Infow("shutdown", "status", "stopping database support", "addr", config.DB.MySQLAddr)
		db.Close()
	}()

	// Start API Service

	log.Infow("startup", "status", "initializing V1 API support")

	// Construct the mux for the API calls.
	apiMux := handlers.APIMux(handlers.APIMuxConfig{
		Log:  log,
		Auth: auth,
		DB:   db,
	}, handlers.WithCORS("*"))

	// Construct a server to service the requests against the mux.
	api := http.Server{
		Addr:     ":8080",
		Handler:  apiMux,
		ErrorLog: zap.NewStdLog(log.Desugar()),
	}

	if err := api.ListenAndServe(); err != nil {
		return err
	}

	return nil
}
