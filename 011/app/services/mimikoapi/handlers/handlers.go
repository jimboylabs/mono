package handlers

import (
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
	v1 "mimiko/app/services/mimikoapi/handlers/v1"
	"mimiko/business/web/auth"
	"mimiko/business/web/v1/mid"
	"mimiko/foundation/web"
	"net/http"
)

type Options struct {
	corsOrigin string
}

func WithCORS(origin string) func(opts *Options) {
	return func(opts *Options) {
		opts.corsOrigin = origin
	}
}

type APIMuxConfig struct {
	Log  *zap.SugaredLogger
	Auth *auth.Auth
	DB   *sqlx.DB
}

func APIMux(config APIMuxConfig, options ...func(opts *Options)) http.Handler {
	var opts Options
	for _, option := range options {
		option(&opts)
	}

	var app *web.App

	if opts.corsOrigin != "" {
		app = web.NewApp(
			mid.Logger(config.Log),
			mid.Errors(config.Log),
			mid.Cors(opts.corsOrigin),
			mid.Panics(),
		)
	} else {
		app = web.NewApp(
			mid.Logger(config.Log),
			mid.Errors(config.Log),
			mid.Panics(),
		)
	}

	// Load the v1 routes.
	v1.Routes(app, v1.Config{
		Log:  config.Log,
		Auth: config.Auth,
		DB:   config.DB,
	})

	return app
}
