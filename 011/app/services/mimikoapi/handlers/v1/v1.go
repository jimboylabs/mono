package v1

import (
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
	"mimiko/app/services/mimikoapi/handlers/v1/medicalrecordgrp"
	"mimiko/app/services/mimikoapi/handlers/v1/patientgrp"
	"mimiko/app/services/mimikoapi/handlers/v1/usergrp"
	"mimiko/business/core/medicalrecords"
	"mimiko/business/core/patient"
	"mimiko/business/core/user"
	"mimiko/business/web/auth"
	"mimiko/business/web/v1/mid"
	"mimiko/foundation/web"
	"net/http"
)

type Config struct {
	Log  *zap.SugaredLogger
	Auth *auth.Auth
	DB   *sqlx.DB
}

func Routes(app *web.App, config Config) {
	const version = "api/v1"

	authen := mid.Authenticate(config.Auth)
	admin := mid.Authorize(auth.RoleAdmin)

	hh := Handlers{}

	app.Handle(http.MethodGet, version, "/hello", hh.Index)
	app.Handle(http.MethodGet, version, "/hello/user", hh.Index, authen)
	app.Handle(http.MethodGet, version, "/hello/admin", hh.Index, authen, admin)

	ugh := usergrp.Handlers{
		User: user.NewCore(config.Log, config.DB),
		Auth: config.Auth,
	}

	app.Handle(http.MethodGet, version, "/users/token", ugh.Token)
	app.Handle(http.MethodGet, version, "/users", ugh.Query, authen)

	patientCore := patient.NewCore(config.Log, config.DB)

	pgh := patientgrp.Handlers{Patient: patientCore}

	app.Handle(http.MethodGet, version, "/patients", pgh.Query, authen)
	app.Handle(http.MethodPost, version, "/patients", pgh.Create, authen, admin)
	app.Handle(http.MethodGet, version, "/patients/{id}", pgh.QueryByID, authen, admin)
	app.Handle(http.MethodPut, version, "/patients/{id}", pgh.Update, authen, admin)
	app.Handle(http.MethodDelete, version, "/patients/{id}", pgh.Delete, authen, admin)

	nmrh := medicalrecordgrp.Handlers{
		MedicalRecord: medicalrecords.NewCore(config.Log, config.DB),
		Patient:       patientCore,
	}

	app.Handle(http.MethodGet, version, "/medical_records", nmrh.Query, authen)
	app.Handle(http.MethodPost, version, "/medical_records", nmrh.Create, authen)
}
