package medicalrecordgrp

import (
	"context"
	"fmt"
	"mimiko/business/core/medicalrecords"
	"mimiko/business/core/patient"
	v1Web "mimiko/business/web/v1"
	"mimiko/foundation/web"
	"net/http"
	"strconv"
)

type Handlers struct {
	MedicalRecord medicalrecords.Core
	Patient       patient.Core
}

func (h Handlers) Query(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	page := r.URL.Query().Get("page")
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid page format [%s]", page), http.StatusBadRequest)
	}

	rows := r.URL.Query().Get("rows")
	rowsPerPage, err := strconv.Atoi(rows)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid rows format [%s]", rows), http.StatusBadRequest)
	}

	medicalRecords, total, err := h.MedicalRecord.Query(ctx, pageNumber, rowsPerPage)
	if err != nil {
		return fmt.Errorf("unable to query for medical_records: %w", err)
	}
	web.SetTotalCount(ctx, total)

	return web.Respond(ctx, w, medicalRecords, http.StatusOK)
}

func (h Handlers) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	var nmr medicalrecords.NewMedicalRecord
	if err := web.Decode(r, &nmr); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	if _, err := h.Patient.QueryByID(ctx, nmr.PatientID); err != nil {
		return fmt.Errorf("querying patient[%s]: %w", nmr.PatientID, err)
	}

	medicalRecord, err := h.MedicalRecord.Create(ctx, nmr, v.Now)
	if err != nil {
		return fmt.Errorf("medical_record[%v]: %w", &medicalRecord, err)
	}

	return web.Respond(ctx, w, medicalRecord, http.StatusCreated)
}
