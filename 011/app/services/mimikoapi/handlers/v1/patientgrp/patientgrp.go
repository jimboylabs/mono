package patientgrp

import (
	"context"
	"errors"
	"fmt"
	"mimiko/business/core/patient"
	"mimiko/business/web/auth"
	v1Web "mimiko/business/web/v1"
	"mimiko/foundation/web"
	"net/http"
	"strconv"
	"strings"
)

type Handlers struct {
	Patient patient.Core
}

func (h Handlers) queryByIDs(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	patientID := r.URL.Query().Get("ids")
	split := strings.Split(patientID, ",")

	var patientIDs []string
	for _, s := range split {
		patientIDs = append(patientIDs, s)
	}

	patients, err := h.Patient.QueryByCriteria(ctx, patientIDs)
	if err != nil {
		return fmt.Errorf("unable to query for patients: %w", err)
	}

	return web.Respond(ctx, w, patients, http.StatusOK)
}

func (h Handlers) Query(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	if r.URL.Query().Has("ids") {
		return h.queryByIDs(ctx, w, r)
	}

	page := r.URL.Query().Get("page")
	pageNumber, err := strconv.Atoi(page)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid page format [%s]", page), http.StatusBadRequest)
	}

	rows := r.URL.Query().Get("rows")
	rowsPerPage, err := strconv.Atoi(rows)
	if err != nil {
		return v1Web.NewRequestError(fmt.Errorf("invalid rows format [%s]", rows), http.StatusBadRequest)
	}

	patients, total, err := h.Patient.Query(ctx, pageNumber, rowsPerPage)
	if err != nil {
		return fmt.Errorf("unable to query for patients: %w", err)
	}

	web.SetTotalCount(ctx, total)

	return web.Respond(ctx, w, patients, http.StatusOK)
}

func (h Handlers) QueryByID(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	id := web.Param(r, "id")

	pat, err := h.Patient.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, patient.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("querying patient[%s]: %w", id, err)
		}
	}

	return web.Respond(ctx, w, pat, http.StatusOK)
}

func (h Handlers) Create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	var np patient.NewPatient
	if err := web.Decode(r, &np); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	patient, err := h.Patient.Create(ctx, np, v.Now)
	if err != nil {
		return fmt.Errorf("patient[%v]: %w", &patient, err)
	}

	return web.Respond(ctx, w, patient, http.StatusCreated)
}

func (h Handlers) Update(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	v, err := web.GetValues(ctx)
	if err != nil {
		return web.NewShutdownError("web value missing from context")
	}

	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	id := web.Param(r, "id")

	var update patient.UpdatePatient
	if err := web.Decode(r, &update); err != nil {
		return fmt.Errorf("unable to decode payload: %w", err)
	}

	_, err = h.Patient.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, patient.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("querying patient[%s]: %w", id, err)
		}
	}

	if !claims.Authorized(auth.RoleAdmin) {
		return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	if err := h.Patient.Update(ctx, id, update, v.Now); err != nil {
		switch {
		case errors.Is(err, patient.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNotFound)
		default:
			return fmt.Errorf("ID[%s] Patient[%v]: %w", id, &update, err)
		}
		return err
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}

func (h Handlers) Delete(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	claims, err := auth.GetClaims(ctx)
	if err != nil {
		return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	id := web.Param(r, "id")

	_, err = h.Patient.QueryByID(ctx, id)
	if err != nil {
		switch {
		case errors.Is(err, patient.ErrNotFound):
			return v1Web.NewRequestError(err, http.StatusNoContent)
		default:
			return fmt.Errorf("querying patient[%s]: %w", id, err)
		}
	}

	if !claims.Authorized(auth.RoleAdmin) {
		return v1Web.NewRequestError(auth.ErrForbidden, http.StatusForbidden)
	}

	if err := h.Patient.Delete(ctx, id); err != nil {
		return fmt.Errorf("ID[%s]: %w", id, err)
	}

	return web.Respond(ctx, w, nil, http.StatusNoContent)
}
