package v1

import (
	"context"
	"fmt"
	"mimiko/foundation/web"
	"net/http"
)

type Handlers struct {
}

func (h Handlers) Index(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	name := "mimiko"

	data := struct {
		Message string `json:"message"`
	}{
		Message: fmt.Sprintf("hello %s", name),
	}

	return web.Respond(ctx, w, data, http.StatusOK)
}
