package commands

import (
	"context"
	"errors"
	"fmt"
	"mimiko/business/data/dbschema"
	"mimiko/business/sys/database"
	"time"
)

var ErrHelp = errors.New("provided help")

func Migrate(config database.Config) error {
	db, err := database.Open(config)
	if err != nil {
		return fmt.Errorf("connect database: %w", err)
	}
	defer db.Close()

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := dbschema.Migrate(ctx, db); err != nil {
		return fmt.Errorf("migrate database: %w", err)
	}

	fmt.Println("migration complete")

	return nil
}
