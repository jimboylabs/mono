package commands

import (
	"context"
	"fmt"
	"go.uber.org/zap"
	"mimiko/business/core/user"
	"mimiko/business/sys/database"
	"time"
)

func UserAdd(log *zap.SugaredLogger, config database.Config, name string, email string, password string) error {
	if email == "" {
		fmt.Println("help: useradd <email> <password>")
		return ErrHelp
	}

	db, err := database.Open(config)
	if err != nil {
		return fmt.Errorf("connect database: %w", err)
	}
	defer db.Close()

	core := user.NewCore(log, db)

	nu := user.NewUser{
		Name:            name,
		Email:           email,
		Password:        password,
		PasswordConfirm: password,
	}

	usr, err := core.Create(context.Background(), nu, time.Now())
	if err != nil {
		return fmt.Errorf("create user: %w", err)
	}

	fmt.Println("user id: ", usr.ID)
	return nil
}
