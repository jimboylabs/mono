package main

import (
	"errors"
	"fmt"
	"github.com/ardanlabs/conf/v3"
	"go.uber.org/zap"
	"mimiko/app/tooling/mimikoadmin/commands"
	"mimiko/business/sys/database"
	"mimiko/foundation/logger"
	"os"
)

func main() {
	log, err := logger.New("MIMIKO-ADMIN")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer log.Sync()

	if err := run(log); err != nil {
		log.Sync()
		fmt.Println(err)
		os.Exit(1)
	}
}

var build = "develop"

func run(log *zap.SugaredLogger) error {
	config := struct {
		conf.Version
		Args conf.Args
		DB   struct {
			MySQLAddr string `conf:"env:MYSQL_ADDR,default:mysql://mimiko:mimiko@192.168.8.212/mimiko"`
		}
	}{
		Version: conf.Version{
			Build: build,
			Desc:  "copyright information here",
		},
	}

	const prefix = "MIMIKO"
	help, err := conf.Parse(prefix, &config)
	if err != nil {
		if errors.Is(err, conf.ErrHelpWanted) {
			fmt.Println(help)
			return nil
		}
		return fmt.Errorf("parsing config: %w", err)
	}

	out, err := conf.String(&config)
	if err != nil {
		return fmt.Errorf("parsing config: %w", err)
	}
	log.Infow("startup", "config", out)

	dbConfig := database.Config{URL: config.DB.MySQLAddr}

	return processCommand(config.Args, log, dbConfig)
}

func processCommand(args conf.Args, log *zap.SugaredLogger, dbConfig database.Config) error {
	switch args.Num(0) {
	case "migrate":
		if err := commands.Migrate(dbConfig); err != nil {
			return fmt.Errorf("migrating database: %w", err)
		}

	case "useradd":
		name := args.Num(1)
		email := args.Num(2)
		password := args.Num(3)

		if err := commands.UserAdd(log, dbConfig, name, email, password); err != nil {
			return fmt.Errorf("adding user: %w", err)
		}

	default:
		return commands.ErrHelp
	}

	return nil
}
