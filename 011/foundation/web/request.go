package web

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
)

func Param(r *http.Request, key string) string {
	vars := mux.Vars(r)
	return vars[key]
}

func Decode(r *http.Request, val any) error {
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(val); err != nil {
		return err
	}

	return nil
}
