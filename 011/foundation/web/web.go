package web

import (
	"context"
	"github.com/gorilla/mux"
	"net/http"
	"time"
)

var _ http.Handler = (*App)(nil)

type Handler func(ctx context.Context, writer http.ResponseWriter, request *http.Request) error

type App struct {
	mux *mux.Router
	mw  []Middleware
}

func NewApp(mw ...Middleware) *App {
	router := mux.NewRouter()
	return &App{mux: router, mw: mw}
}

func (a *App) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	a.mux.ServeHTTP(writer, request)
}

func (a *App) Handle(method string, group string, path string, handler Handler, mw ...Middleware) {

	// First wrap handler specific middleware around this handler.
	handler = wrapMiddleware(mw, handler)

	// Add the application's general middleware to the handler chain.
	handler = wrapMiddleware(a.mw, handler)

	h := func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		v := Values{Now: time.Now().UTC()}

		ctx = context.WithValue(ctx, key, &v)

		if err := handler(ctx, w, r); err != nil {
			return
		}
	}

	finalPath := path
	if finalPath != "" {
		finalPath = "/" + group + path
	}

	a.mux.HandleFunc(finalPath, h).Methods(method)
}
