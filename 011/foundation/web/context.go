package web

import (
	"context"
	"errors"
	"time"
)

type ctxkey int

const key ctxkey = 1

type Values struct {
	Now time.Time

	StatusCode int
	TotalCount int
}

func GetValues(ctx context.Context) (*Values, error) {
	v, ok := ctx.Value(key).(*Values)
	if !ok {
		return nil, errors.New("web value missing from context")
	}

	return v, nil
}

func SetStatusCode(ctx context.Context, statusCode int) error {
	v, ok := ctx.Value(key).(*Values)
	if !ok {
		return errors.New("web value missing from context")
	}
	v.StatusCode = statusCode

	return nil
}

func SetTotalCount(ctx context.Context, count int) error {
	v, ok := ctx.Value(key).(*Values)
	if !ok {
		return errors.New("web value missing from context")
	}
	v.TotalCount = count

	return nil
}