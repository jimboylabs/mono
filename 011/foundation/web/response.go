package web

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

func Respond(ctx context.Context, w http.ResponseWriter, data any, statusCode int) error {
	SetStatusCode(ctx, statusCode)

	v, err := GetValues(ctx)
	if err != nil {
		return err
	}

	w.Header().Set("X-Total-Count", fmt.Sprintf("%d", v.TotalCount))

	if statusCode == http.StatusNoContent {
		w.WriteHeader(statusCode)
		return nil
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(statusCode)

	if _, err := w.Write(jsonData); err != nil {
		return err
	}

	return nil
}
