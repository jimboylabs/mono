create table if not exists users
(
    id              bigint(11)   null primary key auto_increment,
    name            varchar(45)  not null,
    email           varchar(45)  not null,
    phone           varchar(45)  null,
    hashed_password varchar(128) null,

    created_at      datetime     not null default current_timestamp,
    updated_at      datetime     null on update current_timestamp
);