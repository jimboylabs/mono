create table if not exists medical_records
(
    id             integer                            not null primary key auto_increment,
    patient_id     integer,
    physical_check text                               null,

    created_at     datetime default current_timestamp not null,
    updated_at     datetime                           null on update current_timestamp,
    deleted_at     datetime                           null,

    constraint fk_patient_id foreign key (patient_id) references patients (id)
);