create table if not exists patients
(
    id               integer     not null primary key auto_increment,
    name             varchar(45) not null,
    microchip_number varchar(45),
    age              varchar(10),
    gender           varchar(10) not null,
    status           varchar(20) null,

    created_at       datetime default current_timestamp not null,
    updated_at       datetime null on update current_timestamp,
    deleted_at       datetime    null
);