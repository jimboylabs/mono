package dbschema

import (
	"context"
	"embed"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/mysql"
	"github.com/golang-migrate/migrate/v4/source"
	"github.com/golang-migrate/migrate/v4/source/httpfs"
	"github.com/jmoiron/sqlx"
	"mimiko/business/sys/database"
	"net/http"
)

var (
	//go:embed *.sql
	schemaDoc embed.FS
)

type driver struct {
	httpfs.PartialDriver
}

func (d *driver) Open(url string) (source.Driver, error) {
	err := d.PartialDriver.Init(http.FS(schemaDoc), ".")
	if err != nil {
		return nil, err
	}

	return d, nil
}

func init() {
	source.Register("embed", &driver{})
}

func Migrate(ctx context.Context, db *sqlx.DB) error {
	if err := database.StatusCheck(ctx, db); err != nil {
		return fmt.Errorf("status check database: %w", err)
	}

	driver, err := mysql.WithInstance(db.DB, &mysql.Config{})
	if err != nil {
		return fmt.Errorf("load the driver: %w", err)
	}

	m, err := migrate.NewWithDatabaseInstance("embed://", "mysql", driver)
	if err != nil {
		return fmt.Errorf("construct the migration: %w", err)
	}

	return m.Up()
}
