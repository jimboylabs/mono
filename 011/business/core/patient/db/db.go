package db

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
	"mimiko/business/sys/database"
)

type Store struct {
	log          *zap.SugaredLogger
	tr           database.Transactor
	db           sqlx.ExtContext
	isWithinTran bool
}

func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		log: log,
		tr:  db,
		db:  db,
	}
}

func (s Store) WithinTran(ctx context.Context, fn func(sqlx.ExtContext) error) error {
	if s.isWithinTran {
		return fn(s.db)
	}

	return database.WithinTran(ctx, s.log, s.tr, fn)
}

func (s Store) Tran(tx sqlx.ExtContext) Store {
	return Store{
		log:          s.log,
		tr:           s.tr,
		db:           tx,
		isWithinTran: true,
	}
}

func (s Store) Create(ctx context.Context, patient Patient) error {
	const q = `
	INSERT INTO patients
		(name, microchip_number, age, gender, status)
	VALUES 
		(:name, :microchip_number, :age, :gender, :status)`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, patient); err != nil {
		return fmt.Errorf("inserting patient: %w", err)
	}

	return nil
}

func (s Store) Query(ctx context.Context, pageNumber, rowsPerPage int) ([]Patient, int, error) {
	data := struct {
		Offset      int `db:"offset"`
		RowsPerPage int `db:"rows_per_page"`
	}{
		Offset:      (pageNumber - 1) * rowsPerPage,
		RowsPerPage: rowsPerPage,
	}

	const q = `
	SELECT
		id
		, name
		, microchip_number
		, age
		, gender
		, status
		, created_at
		, updated_at
	FROM
		patients
	WHERE deleted_at IS NULL
	ORDER BY id DESC
	LIMIT :offset, :rows_per_page`

	var patients []Patient
	if err := database.NamedQuerySlice(ctx, s.log, s.db, q, data, &patients); err != nil {
		return nil, 0, fmt.Errorf("selecting patients: %w", err)
	}

	const countQ = `
	SELECT
		COUNT(*) as total
	FROM
		patients
	WHERE deleted_at IS NULL`

	count := struct {
		Total int `db:"total"`
	}{
		Total: 0,
	}

	if err := database.NamedQueryStruct(ctx, s.log, s.db, countQ, data, &count); err != nil {
		return nil, 0, fmt.Errorf("counting patients: %w", err)
	}

	return patients, count.Total, nil
}

func (s Store) QueryByID(ctx context.Context, patientID string) (Patient, error) {
	data := struct {
		PatientID string `db:"patient_id"`
	}{
		PatientID: patientID,
	}

	const q = `
	SELECT 
		id
		, name
		, microchip_number
		, age
		, gender
		, status
		, created_at
		, updated_at
	FROM
		patients
	WHERE deleted_at IS NULL AND id = :patient_id`

	var patient Patient
	if err := database.NamedQueryStruct(ctx, s.log, s.db, q, data, &patient); err != nil {
		return Patient{}, fmt.Errorf("selecting patient patientID[%q]: %w", patientID, err)
	}

	return patient, nil
}

func (s Store) Update(ctx context.Context, patient Patient) error {
	const q = `
	UPDATE 
		patients
	SET
		name = :name
		, microchip_number = :microchip_number
		, age = :age
		, gender = :gender
		, status = :status
	WHERE 
		id = :id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, patient); err != nil {
		return fmt.Errorf("updating patient patientID[%s]: %w", patient.ID, err)
	}

	return nil
}

func (s Store) Delete(ctx context.Context, patientID string) error {
	data := struct {
		PatientID string `db:"patient_id"`
	}{
		PatientID: patientID,
	}

	const q = `
	UPDATE
		patients
	SET 
		deleted_at = date('now')
	WHERE id = :patient_id`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, data); err != nil {
		return fmt.Errorf("deleting patient patientID[%s]: %w", patientID, err)
	}

	return nil
}

func (s Store) QueryByCriteria(ctx context.Context, criteria QueryCriteria) ([]Patient, error) {
	var q = `
	SELECT 
		id
		, name
		, microchip_number
		, age
		, gender
		, status
		, created_at
		, updated_at
	FROM
		patients
	WHERE deleted_at IS NULL`

	if len(criteria.PatientIDs) > 0 {
		q += ` AND id IN(:patient_ids)`
	}

	q, args, err := sqlx.In(q, criteria)
	if err != nil {
		return nil, fmt.Errorf("preparing query: %w", err)
	}

	q = s.db.Rebind(q)

	var patients []Patient
	if err := database.NamedQuerySlice(ctx, s.log, s.db, q, args, &patients); err != nil {
		return nil, fmt.Errorf("selecting patients: %w", err)
	}

	return patients, nil
}
