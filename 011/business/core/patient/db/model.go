package db

import "time"

type Patient struct {
	ID              string     `db:"id"`
	Name            string     `db:"name"`
	MicrochipNumber *string    `db:"microchip_number"`
	Age             *string    `db:"age"`
	Gender          string     `db:"gender"`
	Status          *string    `db:"status"`
	CreatedAt       time.Time  `db:"created_at"`
	UpdatedAt       *time.Time `db:"updated_at"`
}

type QueryCriteria struct {
	PatientIDs []string `db:"patient_ids"`
}
