package patient

import (
	"context"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
	"mimiko/business/core/patient/db"
	"mimiko/business/sys/database"
	"time"
)

var (
	ErrNotFound = errors.New("patient not found")
)

type Core struct {
	store db.Store
}

func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{store: db.NewStore(log, sqlxDB)}
}

func (c Core) Create(ctx context.Context, np NewPatient, now time.Time) (Patient, error) {
	if err := np.Validate(); err != nil {
		return Patient{}, fmt.Errorf("validating data: %w", err)
	}

	dbPatient := db.Patient{
		Name:            np.Name,
		MicrochipNumber: &np.MicrochipNumber,
		Age:             &np.Age,
		Gender:          np.Gender,
		Status:          &np.Status,
	}

	tran := func(tx sqlx.ExtContext) error {
		if err := c.store.Tran(tx).Create(ctx, dbPatient); err != nil {
			return fmt.Errorf("create: %w", err)
		}

		return nil
	}

	if err := c.store.WithinTran(ctx, tran); err != nil {
		return Patient{}, fmt.Errorf("tran: %w", err)
	}

	return toPatient(dbPatient), nil
}

func (c Core) Query(ctx context.Context, pageNumber int, rowsPerPage int) ([]Patient, int, error) {
	dbPatients, total, err := c.store.Query(ctx, pageNumber, rowsPerPage)
	if err != nil {
		return nil, 0, fmt.Errorf("query: %w", err)
	}

	return toPatientSlice(dbPatients), total, nil
}

func (c Core) QueryByCriteria(ctx context.Context, patientIDs []string) ([]Patient, error) {
	dbPatients, err := c.store.QueryByCriteria(ctx, db.QueryCriteria{PatientIDs: patientIDs})
	if err != nil {
		return nil, fmt.Errorf("query by criteria: %w", err)
	}

	return toPatientSlice(dbPatients), nil
}

func (c Core) QueryByID(ctx context.Context, patientID string) (Patient, error) {
	//TODO: validate id

	dbPatient, err := c.store.QueryByID(ctx, patientID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return Patient{}, ErrNotFound
		}
		return Patient{}, fmt.Errorf("query: %w", err)
	}

	return toPatient(dbPatient), nil
}

func (c Core) Update(ctx context.Context, patientID string, up UpdatePatient, now time.Time) error {
	if err := up.Validate(); err != nil {
		return fmt.Errorf("validating data: %w", err)
	}

	dbPatient, err := c.store.QueryByID(ctx, patientID)
	if err != nil {
		if errors.Is(err, database.ErrDBNotFound) {
			return ErrNotFound
		}
		return fmt.Errorf("updating patient patientID[%s]: %w", patientID, err)
	}

	if up.Name != nil {
		dbPatient.Name = *up.Name
	}
	if up.MicrochipNumber != nil {
		dbPatient.MicrochipNumber = up.MicrochipNumber
	}
	if up.Age != nil {
		dbPatient.Age = up.Age
	}
	if up.Gender != nil {
		dbPatient.Gender = *up.Gender
	}
	if up.Status != nil {
		dbPatient.Status = up.Status
	}
	dbPatient.UpdatedAt = &now

	if err := c.store.Update(ctx, dbPatient); err != nil {
		return fmt.Errorf("update: %w", err)
	}

	return nil
}

func (c Core) Delete(ctx context.Context, patientID string) error {
	//TODO: validate patient id

	if err := c.store.Delete(ctx, patientID); err != nil {
		return fmt.Errorf("delete: %w", err)
	}

	return nil
}
