package patient

import (
	"mimiko/business/core/patient/db"
	"time"
)

type Patient struct {
	ID              string `json:"id"`
	Name            string `json:"name"`
	MicrochipNumber string `json:"microchip_number"`
	Age             string `json:"age"`
	Gender          string `json:"gender"`
	Status          string `json:"status"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type NewPatient struct {
	Name            string `json:"name"`
	MicrochipNumber string `json:"microchip_number"`
	Age             string `json:"age"`
	Gender          string `json:"gender"`
	Status          string `json:"status"`
}

func (p NewPatient) Validate() error {
	return nil
}

type UpdatePatient struct {
	Name            *string `json:"name"`
	MicrochipNumber *string `json:"microchip_number"`
	Age             *string `json:"age"`
	Gender          *string `json:"gender"`
	Status          *string `json:"status"`
}

func (p UpdatePatient) Validate() error {
	return nil
}

func toPatient(dbPatient db.Patient) Patient {
	var microchipNumber string

	if dbPatient.MicrochipNumber != nil {
		microchipNumber = *dbPatient.MicrochipNumber
	}

	var age string

	if dbPatient.Age != nil {
		age = *dbPatient.Age
	}

	var updatedAt time.Time

	if dbPatient.UpdatedAt != nil {
		updatedAt = *dbPatient.UpdatedAt
	}

	var status string

	if dbPatient.Status != nil {
		status = *dbPatient.Status
	}

	patient := Patient{
		ID:              dbPatient.ID,
		Name:            dbPatient.Name,
		MicrochipNumber: microchipNumber,
		Age:             age,
		Gender:          dbPatient.Gender,
		Status:          status,
		CreatedAt:       dbPatient.CreatedAt,
		UpdatedAt:       updatedAt,
	}

	return patient
}

func toPatientSlice(dbPatients []db.Patient) []Patient {
	var patients = make([]Patient, len(dbPatients))
	for i, patient := range dbPatients {
		patients[i] = toPatient(patient)
	}

	return patients
}
