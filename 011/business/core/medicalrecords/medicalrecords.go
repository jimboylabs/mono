package medicalrecords

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
	"mimiko/business/core/medicalrecords/db"
	"time"
)

type Core struct {
	store db.Store
}

func NewCore(log *zap.SugaredLogger, sqlxDB *sqlx.DB) Core {
	return Core{store: db.NewStore(log, sqlxDB)}
}

func (c Core) Create(ctx context.Context, nmr NewMedicalRecord, now time.Time) (MedicalRecord, error) {
	if err := nmr.Validate(); err != nil {
		return MedicalRecord{}, fmt.Errorf("validating data: %w", err)
	}

	dbRecord := db.MedicalRecord{
		PatientID:     nmr.PatientID,
		PhysicalCheck: nmr.PhysicalCheck,
		CreatedAt:     now,
	}

	tran := func(tx sqlx.ExtContext) error {
		if err := c.store.Tran(tx).Create(ctx, dbRecord); err != nil {
			return fmt.Errorf("create: %w", err)
		}
		return nil
	}

	if err := c.store.WithinTran(ctx, tran); err != nil {
		return MedicalRecord{}, fmt.Errorf("tran: %w", err)
	}

	return toMedicalRecord(dbRecord), nil
}

func (c Core) Query(ctx context.Context, pageNumber int, rowsPerPage int) ([]MedicalRecord, int, error) {
	dbMedicalRecords, total, err := c.store.Query(ctx, pageNumber, rowsPerPage)
	if err != nil {
		return nil, 0, fmt.Errorf("query: %w", err)
	}

	return toMedicalRecordSlice(dbMedicalRecords), total, nil
}
