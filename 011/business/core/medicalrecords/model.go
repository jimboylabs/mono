package medicalrecords

import (
	"mimiko/business/core/medicalrecords/db"
	"time"
)

type MedicalRecord struct {
	ID            string `json:"id"`
	PatientID     string `json:"patient_id"`
	PhysicalCheck string `json:"physical_check"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

func (r MedicalRecord) Validate() error {
	return nil
}

type NewMedicalRecord struct {
	PatientID     string  `json:"patient_id"`
	PhysicalCheck *string `json:"physical_check"`
}

func (r NewMedicalRecord) Validate() error {
	return nil
}

func toMedicalRecord(dbRecord db.MedicalRecord) MedicalRecord {
	var physhicalCheck string

	if dbRecord.PhysicalCheck != nil {
		physhicalCheck = *dbRecord.PhysicalCheck
	}

	var updatedAt time.Time
	if dbRecord.UpdatedAt != nil {
		updatedAt = *dbRecord.UpdatedAt
	}

	return MedicalRecord{
		ID:            dbRecord.ID,
		PatientID:     dbRecord.PatientID,
		PhysicalCheck: physhicalCheck,
		CreatedAt:     dbRecord.CreatedAt,
		UpdatedAt:     updatedAt,
	}
}

func toMedicalRecordSlice(dbRecords []db.MedicalRecord) []MedicalRecord {
	var records = make([]MedicalRecord, len(dbRecords))
	for i, dbRecord := range dbRecords {
		records[i] = toMedicalRecord(dbRecord)
	}
	return records
}
