package db

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
	"mimiko/business/sys/database"
)

type Store struct {
	log          *zap.SugaredLogger
	tr           database.Transactor
	db           sqlx.ExtContext
	isWithinTran bool
}

func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		log: log,
		tr:  db,
		db:  db,
	}
}

func (s Store) WithinTran(ctx context.Context, fn func(sqlx.ExtContext) error) error {
	if s.isWithinTran {
		return fn(s.db)
	}

	return database.WithinTran(ctx, s.log, s.tr, fn)
}

func (s Store) Tran(tx sqlx.ExtContext) Store {
	return Store{
		log:          s.log,
		tr:           s.tr,
		db:           tx,
		isWithinTran: true,
	}
}

func (s Store) Create(ctx context.Context, record MedicalRecord) error {
	const q = `
	INSERT INTO medical_records
		(patient_id, physical_check)
	VALUES
		(:patient_id, :physical_check)`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, record); err != nil {
		return fmt.Errorf("inserting medical_records: %w", err)
	}

	return nil
}

func (s Store) Query(ctx context.Context, pageNumber int, rowsPerPage int) ([]MedicalRecord, int, error) {
	data := struct {
		Offset      int `db:"offset"`
		RowsPerPage int `db:"rows_per_page"`
	}{
		Offset:      (pageNumber - 1) * rowsPerPage,
		RowsPerPage: rowsPerPage,
	}

	const q = `
	SELECT
		id
		, patient_id
		, physical_check
		, created_at
		, updated_at
		, deleted_at
	FROM
		medical_records
	WHERE deleted_at IS NULL
	ORDER BY id DESC
	LIMIT :offset, :rows_per_page`

	var records []MedicalRecord
	if err := database.NamedQuerySlice(ctx, s.log, s.db, q, data, &records); err != nil {
		return nil, 0, fmt.Errorf("selecting medical_records: %w", err)
	}

	const countQ = `
	SELECT
		COUNT(*) AS total
	FROM
		medical_records
	WHERE deleted_at IS NULL`

	count := struct {
		Total int `db:"total"`
	}{
		Total: 0,
	}

	if err := database.NamedQueryStruct(ctx, s.log, s.db, countQ, data, &count); err != nil {
		return nil, 0, fmt.Errorf("counting medical_records: %w", err)
	}

	return records, count.Total, nil
}
