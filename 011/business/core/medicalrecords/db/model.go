package db

import "time"

type MedicalRecord struct {
	ID            string  `db:"id"`
	PatientID     string  `db:"patient_id"`
	PhysicalCheck *string `db:"physical_check"`

	CreatedAt time.Time  `db:"created_at"`
	UpdatedAt *time.Time `db:"updated_at"`
	DeletedAt *time.Time `db:"deleted_at"`
}
