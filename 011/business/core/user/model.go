package user

import (
	"mimiko/business/core/user/db"
	"time"
)

type User struct {
	ID        string    `json:"id"`
	Name      string    `json:"name"`
	Email     string    `json:"email"`
	Phone     string    `json:"phone"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type NewUser struct {
	Name            string   `json:"name"`
	Email           string   `json:"email"`
	Phone           string   `json:"phone"`
	Roles           []string `json:"roles"`
	Password        string   `json:"password"`
	PasswordConfirm string   `json:"password_confirm"`
}

func toUser(dbUser db.User) User {
	var phone string
	if dbUser.Phone != nil {
		phone = *dbUser.Phone
	}

	var updatedAt time.Time
	if dbUser.UpdatedAt != nil {
		updatedAt = *dbUser.UpdatedAt
	}

	user := User{
		ID:        dbUser.ID,
		Name:      dbUser.Name,
		Email:     dbUser.Email,
		Phone:     phone,
		CreatedAt: dbUser.CreatedAt,
		UpdatedAt: updatedAt,
	}

	return user
}

func toUserSlice(dbUsers []db.User) []User {
	users := make([]User, len(dbUsers))
	for i, dbUser := range dbUsers {
		users[i] = toUser(dbUser)
	}

	return users
}
