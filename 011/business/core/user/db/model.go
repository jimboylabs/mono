package db

import (
	"mimiko/business/web/auth"
	"time"
)

type User struct {
	ID             string     `db:"id"`
	Name           string     `db:"name"`
	Email          string     `db:"email"`
	Phone          *string    `db:"phone"`
	HashedPassword []byte     `db:"hashed_password"`
	CreatedAt      time.Time  `db:"created_at"`
	UpdatedAt      *time.Time `db:"updated_at"`
}

func (u User) Roles() []string {
	if u.Email == "jimmyeatcrab@gmail.com" {
		return []string{auth.RoleAdmin}
	}

	return []string{}
}
