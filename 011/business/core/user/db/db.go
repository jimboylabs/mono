package db

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
	"mimiko/business/sys/database"
)

type Store struct {
	log          *zap.SugaredLogger
	tr           database.Transactor
	db           sqlx.ExtContext
	isWithinTran bool
}

func NewStore(log *zap.SugaredLogger, db *sqlx.DB) Store {
	return Store{
		log: log,
		tr:  db,
		db:  db,
	}
}

func (s Store) WithinTran(ctx context.Context, fn func(sqlx.ExtContext) error) error {
	if s.isWithinTran {
		return fn(s.db)
	}

	return database.WithinTran(ctx, s.log, s.tr, fn)
}

func (s Store) Tran(tx sqlx.ExtContext) Store {
	return Store{
		log:          s.log,
		tr:           s.tr,
		db:           tx,
		isWithinTran: true,
	}
}

func (s Store) Create(ctx context.Context, user User) error {
	const q = `
	INSERT INTO users 
		(name, email, hashed_password)
	VALUES 
		(:name, :email, :hashed_password)`

	if err := database.NamedExecContext(ctx, s.log, s.db, q, user); err != nil {
		return fmt.Errorf("inserting user: %w", err)
	}

	return nil
}

func (s Store) Query(ctx context.Context, pageNumber int, rowsPerPage int) ([]User, error) {
	data := struct {
		Offset      int `db:"offset"`
		RowsPerPage int `db:"rows_per_page"`
	}{
		Offset:      (pageNumber - 1) * rowsPerPage,
		RowsPerPage: rowsPerPage,
	}

	const q = `
	SELECT id
		, name
		, email
		, phone
		, hashed_password
		, created_at
		, updated_at
	FROM users
	ORDER BY id DESC LIMIT :offset, :rows_per_page`

	var users []User
	if err := database.NamedQuerySlice(ctx, s.log, s.db, q, data, &users); err != nil {
		return nil, fmt.Errorf("selecting users: %w", err)
	}

	return users, nil
}

func (s Store) QueryByEmail(ctx context.Context, email string) (User, error) {
	data := struct {
		Email string `db:"email"`
	}{
		Email: email,
	}

	const q = `
	SELECT id
		, name 
		, email 
		, phone 
		, hashed_password 
		, created_at
		, updated_at
	FROM users
	WHERE email = :email`

	var user User
	if err := database.NamedQueryStruct(ctx, s.log, s.db, q, data, &user); err != nil {
		return User{}, fmt.Errorf("selecting email[%q]: %w", email, err)
	}

	return user, nil
}
