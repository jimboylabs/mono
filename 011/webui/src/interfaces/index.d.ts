export interface ICategory {
  id: string;
  title: string;
}

export interface IPost {
  id: string;
  title: string;
  content: string;
  status: "published" | "draft" | "rejected";
  createdAt: string;
  category: ICategory;
}

export interface IUser {
  id: string;
  name: string;
  phone: string;
  created_at: string;
  updated_at: string
}

export interface IPatient {
  id: string;
  name: string;
  microchip_number: string;
  age: string;
  gender: string
  created_at: string;
  updated_at: string;
}

export interface IMedicalRecord {
  id: string;
  patient_id: string;
  physical_check: string;
  created_at: string;
  updated_at: string;
}
