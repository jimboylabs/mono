import {AuthProvider, Refine} from "@pankod/refine-core";
import {ErrorComponent, Layout, LoginPage, notificationProvider, ReadyPage,} from "@pankod/refine-antd";
import "@pankod/refine-antd/dist/styles.min.css";
import routerProvider from "@pankod/refine-react-router-v6";
import {UserList} from "pages/users";
import {PatientList, PatientShow, PatientCreate, PatientEdit} from "./pages/patients";
import {MedicalRecordList} from "./pages/medicalrecords"
import axios, {AxiosInstance} from "axios";
import {DataProvider} from "@pankod/refine-core";
import { stringify} from "query-string";

const axiosInstance = axios.create();

axiosInstance.interceptors.request.use(
    (request) => {
        const token = localStorage.getItem(TOKEN_KEY) || "";

        if (request.headers) {
            request.headers["Authorization"] = `Bearer ${token}`
        } else {
            request.headers = {
                Authorization: `Bearer ${token}`
            }
        }

        return request;
    }
);

const TOKEN_KEY = "refine-auth";

const JsonServer = (apiUrl: string, httpClient: AxiosInstance = axiosInstance): DataProvider => ({
    getList: async ({resource, pagination, filters, sort}) => {
        const url = `${apiUrl}/${resource}`;

        const page = pagination?.current || 1;
        const rows = pagination?.pageSize || 10;

        const params = {
            page,
            rows,
        };

        const {data, headers} = await httpClient.get(url, {params})

        const total = +headers["x-total-count"];

        return {
            data,
            total,
        };
    },

    getMany: async ({resource, ids}) => {
        const url = `${apiUrl}/${resource}`;
        const { data } = await httpClient.get(`${url}?${stringify({id: ids.join(',') })}`)

        return {
            data
        };
    },

    create: async ({resource, variables}) => {
        const url = `${apiUrl}/${resource}`

        const {data} = await httpClient.post(url, variables);

        return {data};
    },

    createMany: async ({ resource, variables }) => {
        const response = await Promise.all(
            variables.map(async (param) => {
                const { data } = await httpClient.post(
                    `${apiUrl}/${resource}`,
                    param,
                );
                return data;
            }),
        );

        return { data: response };
    },

    update: async ({resource, id, variables}) => {
        const url = `${apiUrl}/${resource}/${id}`

        const {data} = await httpClient.put(url, variables);

        return {
            data,
        };
    },

    updateMany: async ({resource, ids, variables}) => {
        const response = await Promise.all(
            ids.map(async (id) => {
                const {data} = await httpClient.patch(
                    `${apiUrl}/${resource}/${id}`,
                    variables,
                );
                return data;
            }),
        );

        return {data: response};
    },

    getOne: async ({resource, id}) => {
        const url = `${apiUrl}/${resource}/${id}`;

        const {data} = await httpClient.get(url);

        return {
            data,
        };
    },

    deleteOne: async ({resource, id, variables}) => {
        const url = `${apiUrl}/${resource}/${id}`;

        const {data} = await httpClient.delete(url, variables);

        return {
            data,
        };
    },

    deleteMany: async ({resource, ids, variables}) => {
        const response = await Promise.all(
            ids.map(async (id) => {
                const {data} = await httpClient.delete(
                    `${apiUrl}/${resource}/${id}`,
                    variables,
                );
                return data;
            }),
        );
        return {data: response};
    },

    getApiUrl: () => {
        return apiUrl;
    },

    custom: async ({url, method, filters, sort, payload, query, headers}) => {
        let requestUrl = `${url}?`;

        if (headers) {
            httpClient.defaults.headers = {
                ...httpClient.defaults.headers,
                ...headers,
            };
        }

        let axiosResponse;
        switch (method) {
            case "put":
            case "post":
            case "patch":
                axiosResponse = await httpClient[method](url, payload);
                break;
            case "delete":
                axiosResponse = await httpClient.delete(url);
                break;
            default:
                axiosResponse = await httpClient.get(requestUrl);
                break;
        }

        const {data} = axiosResponse;

        return Promise.resolve({data});
    },
});

export const mimikoAuthProvider: AuthProvider = {
    login: async ({username, password}) => {

        try {
            const {data} = await axiosInstance.get("/api/v1/users/token", {
                auth: {
                    username: username,
                    password: password,
                }
            })

            localStorage.setItem(TOKEN_KEY, data.token)

            return Promise.resolve();
        } catch (error) {
            console.log(error);
        }

        return Promise.reject();
    },
    logout: () => {
        localStorage.removeItem(TOKEN_KEY);
        return Promise.resolve();
    },
    checkError: () => Promise.resolve(),
    checkAuth: () => {
        const token = localStorage.getItem(TOKEN_KEY);
        if (token) {
            return Promise.resolve();
        }

        return Promise.reject();
    },
    getPermissions: () => Promise.resolve(),
    getUserIdentity: () => {
        const token = localStorage.getItem(TOKEN_KEY);
        if (!token) {
            return Promise.reject();
        }

        return Promise.resolve({
            id: 1,
        });
    },
};

function App() {
    return (
        <Refine
            notificationProvider={notificationProvider}
            Layout={Layout}
            ReadyPage={ReadyPage}
            catchAll={<ErrorComponent/>}
            routerProvider={routerProvider}
            dataProvider={JsonServer("/api/v1", axiosInstance)}
            authProvider={mimikoAuthProvider}
            LoginPage={LoginPage}
            resources={[
                /*
                {
                    name: "posts",
                    list: PostList,
                    create: PostCreate,
                    edit: PostEdit,
                    show: PostShow,
                },
                 */
                {
                    name: "users",
                    list: UserList,
                },
                {
                    name: "patients",
                    list: PatientList,
                    show: PatientShow,
                    create: PatientCreate,
                    edit: PatientEdit,
                },
                {
                    name: "medical_records",
                    list: MedicalRecordList
                }
            ]}
        />
    );
}

export default App;
