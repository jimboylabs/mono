import {IResourceComponentsProps, useMany} from "@pankod/refine-core";
import {
    DateField,
    DeleteButton,
    EditButton,
    List,
    ShowButton,
    Space,
    Table,
    TextField,
    useTable,
} from "@pankod/refine-antd";
import {IMedicalRecord, IPatient} from "interfaces";

export const MedicalRecordList: React.FC<IResourceComponentsProps> = () => {
    const {tableProps} = useTable<IMedicalRecord>({
        initialSorter: [],
    });

    const patientIds =
        tableProps?.dataSource?.map((item) => item.patient_id) ?? [];

    const { data: patientsData, isLoading } = useMany<IPatient>({
        resource: "patients",
        ids: patientIds,
        queryOptions: {
            enabled:patientIds.length > 0,
        },
    });

    console.log({ patientsData });

    return (
        <List>
            <Table {...tableProps} rowKey="id">
                <Table.Column
                    dataIndex="id"
                    key="id"
                    title="ID"
                    render={(value) => <TextField value={value}/>}
                />
                <Table.Column
                    dataIndex="patient_id"
                    key="patient_id"
                    title="Patient ID"
                    render={(value) => <TextField value={value}/>}
                />
                <Table.Column
                    dataIndex="physical_check"
                    key="physical_check"
                    title="Physical Check"
                    render={(value) => <TextField value={value}/>}
                />
                <Table.Column
                    dataIndex="created_at"
                    key="created_at"
                    title="Created At"
                    render={(value) => <DateField value={value} format="LLL"/>}
                />
                <Table.Column<IMedicalRecord>
                    title="Actions"
                    dataIndex="actions"
                    render={(_, record) => (
                        <Space>
                            <EditButton hideText size="small" recordItemId={record.id}/>
                            <ShowButton hideText size="small" recordItemId={record.id}/>
                            <DeleteButton hideText size="small" recordItemId={record.id}/>
                        </Space>
                    )}
                />
            </Table>
        </List>
    );
};
