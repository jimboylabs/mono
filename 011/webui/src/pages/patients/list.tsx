import { IResourceComponentsProps } from "@pankod/refine-core";
import {
    List,
    Table,
    TextField,
    useTable,
    DateField,
    Space,
    EditButton,
    DeleteButton,
    ShowButton,
} from "@pankod/refine-antd";
import {IUser} from "interfaces";

export const PatientList: React.FC<IResourceComponentsProps> = () => {
    const { tableProps } = useTable<IUser>({
        initialSorter: [
        ],
    });

    return (
        <List>
            <Table {...tableProps} rowKey="id">
                <Table.Column
                    dataIndex="id"
                    key="id"
                    title="ID"
                    render={(value) => <TextField value={value} />}
                />
                <Table.Column
                    dataIndex="name"
                    key="name"
                    title="Name"
                    render={(value) => <TextField value={value} />}
                />
                <Table.Column
                    dataIndex="microchip_number"
                    key="microchip_number"
                    title="Microchip Number"
                    render={(value) => <TextField value={value} />}
                />
                <Table.Column
                    dataIndex="gender"
                    key="gender"
                    title="Gender"
                    render={(value) => <TextField value={value} />}
                />
                <Table.Column
                    dataIndex="status"
                    key="status"
                    title="Status"
                    render={(value) => <TextField value={value} />}/>
                <Table.Column
                    dataIndex="created_at"
                    key="created_at"
                    title="Created At"
                    render={(value) => <DateField value={value} format="LLL" />}
                />
                <Table.Column<IUser>
                    title="Actions"
                    dataIndex="actions"
                    render={(_, record) => (
                        <Space>
                            <EditButton hideText size="small" recordItemId={record.id} />
                            <ShowButton hideText size="small" recordItemId={record.id} />
                            <DeleteButton hideText size="small" recordItemId={record.id} />
                        </Space>
                    )}
                />
            </Table>
        </List>
    );
};
