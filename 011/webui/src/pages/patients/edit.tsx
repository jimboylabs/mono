import { IResourceComponentsProps } from "@pankod/refine-core";
import {
    Create,
    Form,
    Input,
    Select,
    useForm,
} from "@pankod/refine-antd";

import {IPatient} from "interfaces";

export const PatientEdit: React.FC<IResourceComponentsProps> = () => {
    const { formProps, saveButtonProps } = useForm<IPatient>();

    return (
        <Create saveButtonProps={saveButtonProps}>
            <Form {...formProps} layout="vertical">
                <Form.Item
                    label="Name"
                    name="name"
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Microchip Number"
                    name="microchip_number"
                    rules={[
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Age"
                    name="age"
                    rules={[
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Gender"
                    name="gender"
                    rules={[
                        {
                            required: true,
                        },
                    ]}
                >
                    <Select
                        options={[
                            {
                                label: "Male",
                                value: "male",
                            },
                            {
                                label: "Female",
                                value: "female",
                            }
                        ]}
                    />
                </Form.Item>

                <Form.Item
                    label="Status"
                    name="status"
                    rules={[
                    ]}
                >
                    <Select
                        options={[
                            {
                                label: "Isolation",
                                value: "isolation",
                            },
                            {
                                label: "Quarantine",
                                value: "quarantine",
                            },
                            {
                                label: "Resocialization",
                                value: "resocialization"
                            }
                        ]}
                    />
                </Form.Item>
            </Form>
        </Create>
    );
};
