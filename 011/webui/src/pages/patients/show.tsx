import { IResourceComponentsProps, useShow } from "@pankod/refine-core";
import { Show, Typography} from "@pankod/refine-antd";

import { IPatient } from "interfaces";

const { Title, Text } = Typography;

export const PatientShow: React.FC<IResourceComponentsProps> = () => {
    const { queryResult } = useShow<IPatient>();
    const { data, isLoading } = queryResult;
    const record = data?.data;

    return (
        <Show isLoading={isLoading}>
            <Title level={5}>Name</Title>
            <Text>{record?.name}</Text>

            <Title level={5}>Microchip Number</Title>
            <Text>{record?.microchip_number}</Text>

            <Title level={5}>Age</Title>
            <Text>{record?.age}</Text>

            <Title level={5}>Gender</Title>
            <Text>{record?.gender}</Text>
        </Show>
    );
};
