import { IResourceComponentsProps } from "@pankod/refine-core";
import {
    List,
    Table,
    TextField,
    useTable,
    DateField,
    Space,
    EditButton,
    DeleteButton,
    ShowButton,
} from "@pankod/refine-antd";
import {IUser} from "interfaces";

export const UserList: React.FC<IResourceComponentsProps> = () => {
    const { tableProps } = useTable<IUser>({
        initialSorter: [
        ],
    });

    return (
        <List>
            <Table {...tableProps} rowKey="id">
                <Table.Column
                    dataIndex="id"
                    key="id"
                    title="ID"
                    render={(value) => <TextField value={value} />}
                />
                <Table.Column
                    dataIndex="name"
                    key="name"
                    title="Name"
                    render={(value) => <TextField value={value} />}
                />
                <Table.Column
                    dataIndex="email"
                    key="email"
                    title="Email"
                    render={(value) => <TextField value={value} />}
                />
                <Table.Column
                    dataIndex="createdAt"
                    key="createdAt"
                    title="Created At"
                    render={(value) => <DateField value={value} format="LLL" />}
                />
                <Table.Column<IUser>
                    title="Actions"
                    dataIndex="actions"
                    render={(_, record) => (
                        <Space>
                            <EditButton hideText size="small" recordItemId={record.id} />
                            <ShowButton hideText size="small" recordItemId={record.id} />
                            <DeleteButton hideText size="small" recordItemId={record.id} />
                        </Space>
                    )}
                />
            </Table>
        </List>
    );
};
