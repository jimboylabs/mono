package database

const mysqlStartup = `CREATE TABLE IF NOT EXISTS carts
(
    id         INT(11)     NOT NULL AUTO_INCREMENT,
    cart_id    VARCHAR(20) NOT NULL UNIQUE,
    email      VARCHAR(50) DEFAULT NULL,
    content    JSON        NOT NULL,
    created_at DATETIME    DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME    DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,

    PRIMARY KEY (id)
);
`
