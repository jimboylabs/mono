package rest

import (
	"crypto/tls"
	"fmt"
	"github.com/go-resty/resty/v2"
	"sync"
	"time"
)

var clientOnce struct {
	sync.Once
	c *resty.Client
}

func getClient(source string) *resty.Client {
	clientOnce.Do(func() {
		clientOnce.c = resty.New().
			SetHeader("User-Agent", fmt.Sprintf("comm-api-client-%s", source)).
			SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true}).
			SetRetryCount(0).
			SetTimeout(30 * time.Second).
			EnableTrace().
			OnBeforeRequest(func(client *resty.Client, r *resty.Request) error {
				return nil
			}).
			OnAfterResponse(func(client *resty.Client, r *resty.Response) error {
				return nil
			})
	})

	return clientOnce.c
}
