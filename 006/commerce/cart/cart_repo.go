package cart

import "context"

type CartRepo interface {
	FindByID(ctx context.Context, id string) (Cart, error)
	Update(ctx context.Context, id string, items []Item) error
}
