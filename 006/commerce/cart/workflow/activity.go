package workflow

import (
	"context"
	"errors"
	"gitlab.com/wayanjimmy/commerce/cart"
	"gitlab.com/wayanjimmy/commerce/pkg/gotenberg"
	"gitlab.com/wayanjimmy/commerce/pkg/product"
	"log"
)

type CartActivity struct {
	CartRepo  cart.CartRepo
	Product   product.RESTService
	Gotenberg gotenberg.RESTService
}

type EligibleForCheckoutParam struct {
	Id string
}

func (c *CartActivity) EligibleForCheckout(ctx context.Context, param EligibleForCheckoutParam) error {
	cart, err := c.CartRepo.FindByID(ctx, param.Id)
	if err != nil {
		return err
	}

	if len(cart.Items) == 0 {
		return errors.New("cart items is empty")
	}

	for _, item := range cart.Items {
		_, err := c.Product.FindProductByID(ctx, item.ProductID)
		if err != nil {
			return errors.New("product is not available")
		}
	}

	return nil
}

type GeneratePDFParam struct {
	Url string
}

func (c *CartActivity) GeneratePDF(ctx context.Context, param GeneratePDFParam) (string, error) {
	pdf, err := c.Gotenberg.GeneratePDF(ctx, param.Url)
	if err != nil {
		log.Println(err)
	}

	_ = pdf

	return "SUCCEED", nil
}
