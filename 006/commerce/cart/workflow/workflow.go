package workflow

import (
	"go.temporal.io/sdk/workflow"
	"time"
)

func CheckoutWorkflow(ctx workflow.Context, id string) (string, error) {
	ao := workflow.ActivityOptions{
		StartToCloseTimeout: time.Second * 2,
	}
	ctx1 := workflow.WithActivityOptions(ctx, ao)

	param := EligibleForCheckoutParam{Id: id}

	// step 1
	var a *CartActivity
	err := workflow.ExecuteActivity(ctx1, a.EligibleForCheckout, param).Get(ctx1, nil)
	if err != nil {
		return "", err
	}

	// step 2
	ao = workflow.ActivityOptions{
		StartToCloseTimeout: time.Second * 10,
	}
	ctx2 := workflow.WithActivityOptions(ctx1, ao)

	param2 := GeneratePDFParam{Url: "https://sparksuite.github.io/simple-html-invoice-template"}
	var pdf string
	err = workflow.ExecuteActivity(ctx1, a.GeneratePDF, param2).Get(ctx2, &pdf)
	if err != nil {
		return "", err
	}

	return "COMPLETED", nil
}
