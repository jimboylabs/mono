package main

import (
	"gitlab.com/wayanjimmy/commerce/cart"
	"gitlab.com/wayanjimmy/commerce/cart/workflow"
	"gitlab.com/wayanjimmy/commerce/database"
	"gitlab.com/wayanjimmy/commerce/pkg/gotenberg"
	"gitlab.com/wayanjimmy/commerce/pkg/product"
	"log"

	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/worker"
)

func main() {
	client, err := client.Dial(client.Options{
		HostPort: "host.k3d.internal:7233",
	})
	if err != nil {
		log.Fatalln("unable to create Temporal client", err)
	}
	defer client.Close()

	// open db connection
	dbModule, err := database.New("mysql", "root:root@tcp(host.k3d.internal:3306)/commerce?parseTime=true")
	if err != nil {
		log.Fatalln("unable to connect to database", err)
	}

	cartRepo := &cart.CartRepoMySQL{DB: dbModule}

	productClient := product.RESTService{
		BaseURL: "http://be-product.default.svc.cluster.local",
		Source:  "cart",
	}

	gotenbergClient := gotenberg.RESTService{
		BaseURL: "http://gotenberg.default.svc.cluster.local",
		Source:  "cart",
	}

	a := workflow.CartActivity{
		CartRepo:  cartRepo,
		Product:   productClient,
		Gotenberg: gotenbergClient,
	}

	w := worker.New(client, workflow.CartTaskQueue, worker.Options{})
	w.RegisterActivity(a.EligibleForCheckout)
	w.RegisterActivity(a.GeneratePDF)

	w.RegisterWorkflow(workflow.CheckoutWorkflow)

	err = w.Run(worker.InterruptCh())
	if err != nil {
		log.Fatalln()
	}
}
