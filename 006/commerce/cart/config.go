package cart

type Config struct {
	DBHostPort string
	DBName     string
}
