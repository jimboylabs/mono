//go:generate happy
package main

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/wayanjimmy/commerce/cart"
	"gitlab.com/wayanjimmy/commerce/cart/workflow"
	"gitlab.com/wayanjimmy/commerce/database"
	"gitlab.com/wayanjimmy/commerce/pkg/gotenberg"
	"gitlab.com/wayanjimmy/commerce/pkg/product"
	"go.temporal.io/sdk/client"
	"log"
	"net/http"
)

// An Error that implements http.Handler to write structured JSON errors.
type Error struct {
	code    int
	message string
}

func Errorf(code int, format string, args ...interface{}) error {
	return Error{code, fmt.Sprintf(format, args...)}
}

func (e Error) Error() string { return e.message }

func (e Error) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(e.code)
	json.NewEncoder(w).Encode(map[string]string{"error": e.message})
}

type Service struct {
	temporal  client.Client
	cartRepo  cart.CartRepo
	product   product.RESTService
	gotenberg gotenberg.RESTService
}

//happy:api GET /cart/:id
func (s *Service) GetCart(id string) (cart.Cart, error) {
	if id == "" {
		return cart.Cart{}, Errorf(http.StatusNotFound, "id can't be empty")
	}

	c, err := s.cartRepo.FindByID(context.Background(), id)
	if err != nil {
		return cart.Cart{}, Errorf(http.StatusNotFound, "cart %q not found", id)
	}

	if len(c.Items) == 0 {
		c.Items = make([]cart.Item, 0)
	}

	return c, nil
}

//happy:api POST /cart/:id/add
func (s *Service) CreateItem(id string, item cart.Item) error {
	ctx := context.Background()

	if item.ProductID == "" {
		return Errorf(http.StatusBadRequest, "product id can't be empty")
	}

	_, err := s.product.FindProductByID(ctx, item.ProductID)
	if err != nil {
		return Errorf(http.StatusNotFound, "product id %s not found", item.ProductID)
	}

	err = s.cartRepo.Update(ctx, id, []cart.Item{item})
	if err != nil {
		return Errorf(http.StatusInternalServerError, "something went wrong")
	}

	return nil
}

//happy:api POST /cart/:id/checkout
func (s *Service) Checkout(id string, request cart.CheckoutRequest) (cart.CheckoutResponse, error) {
	ctx := context.Background()

	options := client.StartWorkflowOptions{
		ID:        fmt.Sprintf("cart-checkout-%s", id),
		TaskQueue: workflow.CartTaskQueue,
	}

	we, err := s.temporal.ExecuteWorkflow(ctx, options, workflow.CheckoutWorkflow, id)
	if err != nil {
		log.Fatalln("unable to complete workflow", err)
		return cart.CheckoutResponse{}, Errorf(http.StatusInternalServerError, "unable to complete the workflow")
	}

	var result string
	if err := we.Get(ctx, &result); err != nil {
		log.Fatalln("unable to get workflow result")
		return cart.CheckoutResponse{}, Errorf(http.StatusInternalServerError, "unable to get workflow result")
	}

	return cart.CheckoutResponse{OrderNumber: we.GetRunID()}, nil
}

func printResults(greeting string, workflowID, runID string) {
	fmt.Printf("\nWorkflowID: %s RunID: %s\n", workflowID, runID)
	fmt.Printf("\n%s\n\n", greeting)
}

func main() {
	host := "host.k3d.internal"
	//host := "localhost"

	// create temporal client
	c, err := client.Dial(client.Options{
		HostPort: fmt.Sprintf("%s:7233", host),
	})
	if err != nil {
		log.Fatalln("unable to create temporal client", err)
	}
	defer c.Close()

	// open db connection
	dbModule, err := database.New("mysql", fmt.Sprintf("root:root@tcp(%s:3306)/commerce?parseTime=true", host))
	if err != nil {
		log.Fatalln("unable to connect to database", err)
	}

	cartRepo := &cart.CartRepoMySQL{DB: dbModule}

	productClient := product.RESTService{
		BaseURL: "http://be-product.default.svc.cluster.local",
		//BaseURL: "http://localhost:8080",
		Source: "cart",
	}

	gotenbergClient := gotenberg.RESTService{
		BaseURL: "http://gotenberg.default.svc.cluster.local",
		//BaseURL: "http://localhost:8080",
		Source: "cart",
	}

	service := &Service{temporal: c, cartRepo: cartRepo, product: productClient, gotenberg: gotenbergClient}
	http.ListenAndServe(":9000", service)
}
